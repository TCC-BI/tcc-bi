\chapter{Introdução}

As ferramentas de simulação computacional tornaram-se cruciais para o desenvolvimento de um amplo conjunto de tecnologias do cotidiano, por isso desempenham um papel de facilitadoras do 
avanço tecnológico. Uma destas ferramentas é a Dinâmica dos Fluidos Computacional (DFC) \footnote{\textit{Computational Fluid Dynamics} (CFD), em inglês.}, que surgiu no âmbito 
das indústrias aeronáutica e aeroespacial, vindo a ser fomentada posteriormente pela pesquisa acadêmica. Inicialmente a aplicação da DFC se restringia a este ambiente de alta tecnologia,
contudo, veio a tornar-se progressivamente uma metodologia adotada para resolver problemas complexos na prática da engenharia moderna \cite{bib:tu}, mostrando-se uma ferramenta essencial em inúmeras outras áreas da indústria automotiva, de geração energética, química, nuclear e marinha, ou seja, novos ramos da ciência adentraram no rol de usuários da DFC. A indústria eletrônica passou a empregá-la para otimizar sistemas de energia e de transferência de calor, que 
são utilizados no resfriamento de dispositivos eletrônicos. A indústria biomédica adota a DFC como um núcleo de ferramentas de desenvolvimento e validação de aplicações médicas. E a 
indústria da construção aplica DFC em projetos de aquecimento, ventilação, resfriamento de ambientes, simulações de incêndio e na avaliação da qualidade do ar \cite{bib:moukalled}.


A atual popularidade desta técnica computacional pode ser explicada por alguns fatores. Primeiro, devemos ter ciência da complexidade das equações que regem o comportamento dos fluidos. 
As ditas equações de Navier-Stokes modelam --- com acurácia satisfatória --- um amplo conjunto de fenômenos de fluxos, sejam eles turbulentos, laminares, de fluidos monofásicos, bifásicos, compressíveis ou incompressíveis. Por serem Equações Diferenciais Parciais (EDPs) geralmente assumem a forma não-linear, pois em casos práticos algumas considerações a fim de simplificar o problema não podem ser feitas sem afetar a confiabilidade do modelo. Quando este for o caso, a solução analítica das EDPs é inexistente. Esta é uma limitação que começou a ser superada por meio de dois fatores interdependentes: a evolução da capacidade de processamento dos computadores digitais, e o avanço no desenvolvimento teórico dos métodos numéricos (que incidem diretamente sobre as aproximações e soluções das EDPs).

A partir de então, a DFC começou a ganhar espaço no ambiente científico e passou a atuar na linha de frente de investigação na área de transferência de calor e dinâmica dos fluidos. 
\citeonline{bib:tu} elencam alguns motivos que contribuíram para isto, sendo o primeiro deles a oportunidade que a DFC oferece de estudar termos específicos nas equações governantes de forma mais detalhada, assim, novos caminhos para o desenvolvimento teórico são explorados. Em segundo lugar, a DFC complementa a abordagem experimental, podendo utilizar em suas simulações dados coletados experimentalmente, ou até mesmo preceder a técnica experimental, reduzindo substancialmente o tempo e os custos com a concepção/produção de protótipos. E, quando utilizada na fase inicial dos projetos torna possível a investigação de parâmetros que indiquem quais configurações devem ser reproduzidas e testadas experimentalmente. Outro ponto importante a ressaltar é a capacidade da DFC em simular condições de fluxo não reproduzíveis em laboratório, como as encontradas na geofísica, na hemodinâmica, em cenários de acidentes nucleares ou até mesmo cenários de ordem de grandeza espacial elevada. 


Além disso, os resultados providos pela DFC proporcionam uma visualização detalhada e informação compreensível dos fenômenos estudados. E por isso está revolucionando o aprendizado e
o ensino de Mecânica dos Fluidos e Termodinâmica Aplicada nas instituições de ensino superior. Fato este que é alavancado pelo desenvolvimento de pacotes de \textit{softwares}, baseados 
em DFC, que tornam o uso da técnica mais amigável aos estudantes, de maneira que possam reforçar visualmente os conceitos de escoamentos de fluidos e transferência de calor ministrados
em aulas teóricas \cite{bib:tu}. No que tange aos docentes, os \textit{softwares} dão suporte para criarem seus próprios exemplos, ou até mesmo customizar casos pré-existentes, com os 
quais alunos são introduzidos ao uso efetivo da DFC para resolver problemas reais de engenharia. Dentre estes \textit{softwares} está o OpenFOAM.

O OpenFOAM é um software livre, de desenvolvimento colaborativo, em que o usuário pode fazer modificações no código para que este se adeque ao seu caso problema. No OpenFOAM está 
implementada a abordagem do Método dos Volumes Finitos (MVF), que aplica a formulação do volume de controle para discretizar as Equações Diferenciais Parciais (EDPs) governantes 
dos fenômenos de fluxo (as supracitadas Equações de Navier-Stokes). Quando discretizadas, cada EDP se converte em um sistema de equações algébricas, que é fruto de um balanço de 
conservação de uma propriedade para cada volume elementar. Isto implica dizer que teremos múltiplos volumes elementares cobrindo o domínio espacial, isto é a base para o conceito 
de malha. ``Malha é o conjunto de pontos discretos nos quais a solução das equações será conhecida'' \cite{bib:fortuna}. Nos métodos baseados em malha, uma equação discretizada é 
uma relação algébrica que associa os valores da variável dependente com um grupo de pontos da malha, também chamada de domínio computacional.

Como citado anteriormente, uma das aplicações da DFC é na área de hemodinâmica. A Hemodinâmica Computacional (HC) consiste na simulação computacional do escoamento sanguíneo no 
sistema circulatório humano, e tem se mostrado uma ferramenta poderosa, em crescente ascensão nas últimas décadas, porém ainda pouco utilizada em aplicações práticas da biomedicina. 
De acordo com \citeonline{bib:sinnott}, a HC têm um potencial particularmente grande para ajudar a biomedicina a entender doenças cardiovasculares.  

Outros autores, como \citeonline{bib:tu2}, afirmam que a HC é um campo emergente que oferece diversas possibilidades de suporte à biomedicina, como por exemplo, o planejamento 
virtual de cirurgias, avaliação clínica de doenças vasculares e desenvolvimento de dispositivos médicos. \citeonline{bib:tu} listam uma série de outros benefícios da aplicação 
da DFC na área biomédica como, por exemplo, o melhor entendimento dos processos biológicos do organismo humano e o auxílio ao desenvolvimento de melhores procedimentos cirúrgicos. O uso de técnicas computacionais aliado a conhecimentos básicos de biomedicina já se provou uma ferramenta poderosa na compreensão do funcionamento do organismo humano, bem como no desenvolvimento de técnicas cirúrgicas mais eficientes. %Com este trabalho pretende-se fazer um estudo sobre a técnica computacional dos Volumes Finitos, amplamente utilizada para resolver problemas de fluxo, bem como aplicá-la ao estudo do escoamento sanguíneo na bifurcação da artéria carótida, localizada à altura do pescoço e compreender o padrão de escoamento local e como a geometria da bifurcação o afeta, buscando distúrbios que possam indicar o surgimento da aterosclerose.

No corpo do trabalho são explanados os princípios básicos de hemodinâmica, discorrendo-se depois sobre a anatomia da artéria carótida; as equações que governam a dinâmica 
dos escoamentos; o procedimento numérico utilizado para discretizar estas equações; a concepção das geometrias, geração das malhas e o software empregado para solucionar o 
escoamento. Por fim, são apresentados as linhas de corrente, os campos de velocidade e pressão do fluxo na Carótida, os resíduos dos cálculos e as considerações acerca destes resultados.



    


