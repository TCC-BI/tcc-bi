\section{Artéria Carótida} \label{secao:arteria}

As carótidas são um par de artérias que se localizam em ambos os lados do pescoço humano, sendo as principais responsáveis pela condução de sangue para a face e para o cérebro.
\begin{figure}[htb!]{11.5cm}
	\caption{Localização anatômica da artéria carótida}
	\includegraphics[scale=0.55]{carotida2.png}
	\source{Sistema..., \citeyear{anatomia}.} 
	\label{fig:carotida}
\end{figure}
De acordo com a Fig. \ref{fig:carotida}, a carótida tem um ramo principal, chamado de artéria carótida comum (\textit{acc}), que se bifurca em dois outros ramos: a artéria carótida externa (\textit{ace}) e a artéria carótida interna (\textit{aci}). Esta irriga o cérebro e apresenta um alargamento denominado \textit{sinus} ou bulbo, já aquela irriga a face. Segundo \citeonline{bib:guyton}, o cérebro recebe aproximadamente $14 \: \%$ do fluxo sanguíneo total, o que corresponde a cerca de $700 \: ml/min$ de sangue sendo suprido ao cérebro através da carótida.

Esta artéria possui uma grande variedade geométrica, dependendo de variáveis como sexo e idade do indivíduo. \citeonline{bib:ding} catalogaram 74 modelos diferentes de bifurcações; algumas delas são apresentadas na Fig. \ref{fig:tiposcarotidas}.
\begin{figure}[htb!]{13cm}
	\caption{Diferentes tipos de bifurcação da artéria carótida}
	\includegraphics[scale=0.7]{tiposcarotidas.png}
	\source{\citeauthoronline{bib:ding}, \citeyear{bib:ding}.} 
	\label{fig:tiposcarotidas}
\end{figure}
As artérias mostradas correspondem a indivíduos adultos entre $30$ e $75$ anos e foram fotografadas durante procedimentos de autópsia. \citeonline{bib:ding} observaram diferentes dimensões e características na região bifurcada, classificando as artérias em três grupos, cujas características são sintetizados a seguir: 

\begin{enumerate}[label={(\arabic*)}]
	\item A \textit{aci} \footnote{Ramos da esquerda na Fig. \ref{fig:tiposcarotidas}} é reta pelo menos o equivalente a duas vezes o comprimento do \textit{sinus} (ver 1 na Fig. \ref{fig:tiposcarotidas});
	\item A \textit{aci} curva-se após o final do \textit{sinus} (ver 2, 3, 4, 5, 6, 7 e 8 na Fig. \ref{fig:tiposcarotidas});
	\item A \textit{aci} curva-se após a junção das artéria comum e interna (ver 9, 10 e 11 na Fig. \ref{fig:tiposcarotidas}). 
\end{enumerate}   

Ressaltamos que as geometrias simuladas neste trabalho ($2D$ e $3D$) pertencem ao grupo (2) acima, ou seja, a \textit{aci} curva-se após o final do \textit{sinus}. 

Como citado anteriormente,  $14 \: \%$ do fluxo sanguíneo total é direcionado ao cérebro através da carótida, especificamente pelo seu ramo interno, tornando esta um dos principais vasos sanguíneos. Confirmando essa importância, já é sabido que ``a estenose aterosclerótica da artéria carótida, que estreita a bifurcação do pescoço, causa $20 \: \%$ de todos os acidentes vasculares cerebrais isquêmicos e ataques isquêmicos transitórios'' \footnote{Texto original: ``Atherosclerotic stenosis of the carotid artery (CA), narrowing the bifurcation neck, causes $20 \: \%$ of all ischemic strokes and transient ischemic attacks''} \cite[tradução nossa]{bib:auricchio}.

Estenose, em hemodinâmica, é o estreitamento de um vaso sanguíneo causado por alguma condição anômala. A estenose aterosclerótica é, então, o estreitamento do vaso em virtude de uma doença chamada aterosclerose. Segundo os ensinamentos de \citeonline{bib:guyton} a aterosclerose é causada pelo desenvolvimento de depósitos gordurosos e fibróticos nas paredes arteriais; em seu estágio inicial esta doença decorre da lesão ou degeneração das células que revestem a superfície interna da artéria. Conseguintemente, as células musculares que se localizam abaixo das células superficiais lesionadas começam a se replicar, originando uma saliência na parede arterial. No próximo estágio, substâncias gordurosas, em especial o colesterol, começam a se depositar nas células musculares em replicação levando ao aparecimento de uma lesão lipofibrótica, a placa aterosclerótica, que acaba por causar obstrução arterial, impedindo o fluxo sanguíneo.

A aplicação da DFC na biomedicina é uma ferramenta poderosa que vêm sendo cada vez mais explorada no meio científico, em particular ``têm um grande potencial para ajudar a entender os mecanismos responsáveis por doenças cardiovasculares como aterosclerose e formação de trombos'' \footnote{Texto original: ``[...] it has great potential for helping us better understand the mechanisms responsible for cardiovascular disease such as atherosclerosis and thrombi formation.''} \cite[tradução nossa]{bib:sinnott}. O comportamento do escoamento sanguíneo em veias e artérias é fortemente dependente de suas características geométricas, como ramificações, dobras e constrições do vaso. A importância do uso da DFC em simulações da carótida reside no fato de que se for conhecido o padrão de escoamento nessa região e compreendido como a geometria afeta o escoamento, então torna-se possível verificar a presença de distúrbios nos campos de velocidade e pressão. Fatores estes que podem indicar o surgimento da aterosclerose levando, assim, ao diagnóstico precoce desta anomalia.

Diversos autores como \citeonline{bib:ding}, \citeonline{bib:auricchio}, \citeonline{bib:sinnott}, \citeonline{bib:tanaka}, \citeonline{bib:dzwinel} e \citeonline{bib:beleza} corroboram o supracitado ao afirmarem que padrões de escoamento com distúrbio têm um importante papel no surgimento da aterosclerose. Os autores relatam que uma das principais características de regiões bifurcadas é a presença de padrões de escoamento com distúrbio, por isso a bifurcação da carótida é uma das regiões mais suscetíveis à aterosclerose. 

Características geométricas que estes autores ressaltam terem papel central na formação do fluxo sanguíneo na bifurcação da carótida são as seguintes:

\begin{itemize}
	\item Presença de efeitos de curvatura nas junções \textit{acc-aci} e \textit{acc-ace};
	\item Alargamento do diâmetro na entrada da artéria carótida interna, o \textit{sinus}, que abruptamente se estreita na saída;
	\item Assimetria quanto as ramificações das artérias interna e externa (a carótida interna tem um diâmetro maior). 
\end{itemize} 

O \textit{sinus} é uma região crítica para o fluxo sanguíneo local. Este se caracteriza por um alargamento e posterior afunilamento da artéria, exibindo propriedades de um duto curvo na transição da \textit{acc} para \textit{aci}. A Fig. \ref{fig:bifurcacao} mostra a bifurcação de uma artéria carótida.

\begin{figure}[htb!]{10.5cm}
	\caption{Bifurcação da artéria carótida}
	\includegraphics[scale=0.5]{arterianomeada.png}
	\source{Adaptada de \citeauthoronline{bib:beleza}, \citeyear{bib:beleza}.} 
	\label{fig:bifurcacao}
\end{figure}

