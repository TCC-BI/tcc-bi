\section{Equações Governantes} \label{secao:equacoes}

As equações que regem o escoamento de fluidos são deduzidas a partir das leis de conservação da física. Por exemplo, a \textit{equação da continuidade} pode ser obtida aplicando a lei de conservação da massa ao escoamento de um fluido. Já a \textit{equação da conservação do momento} é obtida a partir da segunda lei de Newton, que nos diz que a taxa de variação do momento é igual a soma das forças agindo sobre uma partícula de fluido. E, por sua vez, a \textit{equação da conservação da energia} assume que a variação da energia é igual a soma da taxa de calor adicionada e da taxa de trabalho realizada em uma partícula de fluido, sendo simplesmente a aplicação da primeira lei da termodinâmica \cite{bib:versteeg}.


Todas estas equações serão demonstradas nas subseções seguintes utilizando o sistema de coordenadas cartesianas, e para tal será adotada a abordagem de \citeonline{bib:tobias}, que em seu livro faz uma discussão sobre aspectos matemáticos, numéricos e derivativos utilizados no campo da DFC e no OpenFOAM.

De modo geral, as considerações de \citeonline{bib:versteeg} se enquadram nesta seção, haja vista que fluido será considerado espacialmente contínuo. E para a análise dos fluxos de massa, que se dá em escala macroscópica, a estrutura molecular da matéria e os movimentos moleculares podem ser ignorados. Assim, o comportamento do fluido será descrito em termos de propriedades macroscópicas, tais como velocidade, pressão e massa específica, além das suas derivadas espaciais e temporais. Essas quantidades podem ser calculadas como médias em relação a um número suficientemente grande de moléculas. Logo, por definição, uma partícula ou um ponto no fluido será o menor elemento possível de fluido cujas propriedades macroscópicas não são influenciadas por moléculas individuais.

\subsection{Equação da Continuidade}\label{sec:continuidade}
Esta equação nada mais é do que um balanço de massa sobre um elemento de volume arbitrário. Para provar isto irá se considerar um fluxo de massa através de um pequeno elemento de volume de controle $dV$, e assumir que em seu interior massa não é transformada em energia ou vice-versa. Um balanço de massa, então, pode ser realizado para este elemento de volume. Isto significa dizer que o fluxo de massa que entra através das suas superfícies deve ser igual ao fluxo que sai. Se for levada em consideração a taxa de acúmulo de massa em $dV$, tem-se que:
\begin{equation}
	\hspace{1.5cm} \left[\begin{array}{c}
	\text{taxa de acúmulo de}\\
	\text{ massa dentro de} \ dV
	\end{array}\right]=	
	\left[\begin{array}{c}
	\text{fluxo de massa}\\
	\text{entrando em }dV
	\end{array}\right] -
	\left[\begin{array}{c}
	\text{fluxo de massa}\\
	\text{saindo de }dV
	\end{array}\right].
	\label{continuidadearray}
\end{equation}

É sabido que a massa é transportada através das superfícies pela velocidade. Esse fenômeno de transporte é chamado de convecção \footnote{É comum na literatura encontrarmos o termo \textit{advecção}, ele abrange o fenômeno de forma mais geral.} e ocorre para as três dimensões espaciais $x \ (u_{x})$, $y \ (u_{y})$ e $z \ (u_{z})$. Outro fenômeno que pode ocorrer está relacionado à compressão ou expansão, nesse caso a \textit{massa específica} dentro do elemento irá mudar. Para entender melhor estas implicações da Eq. \eqref{continuidadearray}, deve-se analisar a Fig. \ref{fig:dv}. 

\begin{figure}[htb!]{11cm}
	\caption{Elemento de volume $dV$ e fluxo de massa através de suas faces}
	\includegraphics[scale=0.6]{dv}
	\source{Os autores, 2017.}
	\label{fig:dv}
\end{figure}

Nela pode-se perceber que os vetores de velocidade possuem direção normal às faces. O que chama-se na Eq. \eqref{continuidadearray} de fluxo de massa é simplesmente a \textit{vazão mássica} (sua unidade no SI é o $kg/s$) que corresponde a quantidade em massa de um fluido que escoa através de certa seção de área em um dado intervalo de tempo. Portanto, para o elemento de volume $dV$ tem-se que a vazão mássica em cada face é a massa específica vezes a velocidade na respectiva área: \vspace{0.5cm}
\begin{itemize}
\item Massa entrando em $x$: \ $(\rho u_{x})|_{x} \ \Delta y \Delta z$, \vspace{-0.25cm}
\item Massa saindo de $x$: \ \ \ \ \ $(\rho u_{x})|_{x+\Delta x} \ \Delta y \Delta z$, 
\item Massa entrando em $y$: \ $(\rho u_{y})|_{y} \ \Delta x \Delta z$, \vspace{-0.25cm}
\item Massa saindo de $y$: \ \ \ \ \ $(\rho u_{y})|_{y+\Delta y} \  \Delta x \Delta z$, 
\item Massa entrando em $z$: \ $(\rho u_{z})|_{z} \ \Delta x \Delta y$, \vspace{-0.25cm}
\item Massa saindo de $z$: \ \ \ \ \ $(\rho u_{z})|_{z+\Delta z} \ \Delta x \Delta y$.

\end{itemize} 
Se o fluido for incompressível, tudo que está entrando no elemento está saindo também, assim os termos acima são suficientes para descrever o fluxo em todas as faces.
Em contrapartida, se tivermos um fluido compressível, a taxa de variação para a massa específica $\rho$ é relacionada com o volume --- já que sua unidade no SI é o $kg/m^{3}$ --- e irá variar com relação ao tempo. Dessa forma, pode-se escrever sua taxa de variação:
\begin{equation}
\centering
\frac{\Delta \rho}{\Delta t}.
\label{deltarho}
\end{equation}

Reescrevendo a Eq. \eqref{continuidadearray} fazendo uso da Eq. \eqref{deltarho} e da análise de fluxos nas faces, tem-se que:
\begin{gather*}
\frac{\Delta \rho}{\Delta t} \Delta x \Delta y \Delta z = \big((\rho u_{x})|_{x}-(\rho u_{x})|_{x+ \Delta x} \big) \Delta y \Delta z + \big( (\rho u_{y})|_{y}-(\rho u_{y})|_{y+ \Delta y} \big ) \Delta x \Delta z \\ 
\hspace{2.6cm}+ \big( (\rho u_{z})|_{z}-(\rho u_{z})|_{z+ \Delta z}\big) \Delta x \Delta y. 
\end{gather*}

Multiplicando toda esta equação por $1 / \Delta V$ (e lembrando que $\Delta V = \Delta x \Delta y \Delta z$), ficamos com:
\begin{gather}
\dfrac{\Delta \rho}{\Delta t} = \dfrac{(\rho u_{x})|_{x}-(\rho u_{x})|_{x+ \Delta x}}{\Delta x} + \dfrac{(\rho u_{y})|_{y}-(\rho u_{y})|_{y+ \Delta y}}{\Delta y} + \dfrac{(\rho u_{z})|_{z}-(\rho u_{z})|_{z+ \Delta z}}{\Delta z}.
\label{cmdiferencas}
\end{gather}

Para a derivação da equação de conservação da massa precisa-se assumir um elemento de volume infinitesimal, isto acontece diminuindo-se as distâncias dos cantos do elemento de volume $dV$ de forma que $\Delta \rightarrow 0$:
\begin{equation}
\frac{\Delta}{\Delta x} \rightarrow \lim_{x\to 0}\frac{\Delta}{\Delta x} = \frac{\partial}{\partial x},
\label{infinitesimalx}
\end{equation}

\noindent além disso, o intervalo de tempo também precisa ser infinitesimal, i. e, 
\begin{equation}
\frac{\Delta}{\Delta t} \rightarrow \lim_{t\to 0}\dfrac{\Delta}{\Delta t} = \frac{\partial}{\partial t}. \
\label{infinitesimalt}
\end{equation}

A equação da continuidade é obtida na sua forma diferencial aplicando as Eqs. \eqref{infinitesimalx} e \eqref{infinitesimalt} na Eq. \eqref{cmdiferencas}. Da forma como segue:
\begin{gather}
\dfrac{(\rho u_{x})|_{x}-(\rho u_{x})|_{x+ \Delta x}}{\Delta x} = \dfrac{- \Delta (\rho u_{x})}{\Delta x} \rightarrow  - \frac{\partial}{\partial x}(\rho u_{x}), \nonumber \\
\dfrac{(\rho u_{y})|_{y}-(\rho u_{y})|_{y+ \Delta y}}{\Delta y} = \dfrac{- \Delta (\rho u_{y})}{\Delta y} \rightarrow  - \dfrac{\partial}{\partial y}(\rho u_{y}), \nonumber \\
\dfrac{(\rho u_{z})|_{z}-(\rho u_{z})|_{z+ \Delta z}}{\Delta z} = \dfrac{- \Delta (\rho u_{z})}{\Delta z} \rightarrow  - \dfrac{\partial}{\partial z}(\rho u_{z}), \nonumber \\
\dfrac{\Delta \rho}{\Delta t} \rightarrow \frac{\partial \rho}{\partial t}, \nonumber
\end{gather}

\noindent e finalmente, a forma geral da equação da conservação da massa também dita equação da continuidade (para o caso deste trabalho) é dada por:
\begin{equation}
\dfrac{\partial \rho}{\partial t} = - \left( \dfrac{\partial}{\partial x}(\rho u_{x}) + \dfrac{\partial }{\partial y}(\rho u_{y}) + \dfrac{\partial }{\partial z}(\rho u_{z}) \right).
\label{equacaodacontinuidade}
\end{equation}

A Eq. \eqref{equacaodacontinuidade} é por vezes encontrada com a notação que utiliza o operador nabla $\nabla$ e o vetor de velocidade \textbf{U} \footnote{Em todo o corpo deste trabalho utilizaremos a notação das letras em negrito para indicar que se tratam de vetores ou tensores.} (com as componentes $u_{x}$, $u_{y}$, $u_{z}$):
\begin{equation}
	\frac{\partial \rho}{\partial t} = - \nabla \bullet (\rho \textbf{U}).
	\label{equacaodacontinuidadediv}
\end{equation}

Para finalizar, é montada a formulação integral desta equação. O teorema de Gauss estabelece uma relação entre integrais de volume e de superfície. O teorema estabelece que para um vetor arbitrário $\textbf{a}$ e um vetor $\textbf{n}$ normal à superfície $S$ de um volume é verdadeira a seguinte relação:
\begin{equation}
	\int_{dV} \nabla \bullet (\textbf{a}) \ dV = \int_{S} (\textbf{a}) \cdot \textbf{n} \ dS.
	\label{teoremadegauss}
\end{equation}

Integrando a Eq. \eqref{equacaodacontinuidadediv} sobre o volume $dV$ e aplicando-se este teorema, é possível transformar o termo de divergência em uma integral de superfície. Portanto, para


\hspace{-1.25cm} Fluido compressível, tem-se
\begin{gather}
\frac{\partial}{\partial t} \int \rho \ dV = - \oint \rho \textbf{U} \cdot \textbf{n} \ dS,
\label{eqcontinuidadecompressivel}
\end{gather}

\hspace{-1.25cm} e para Fluido incompressível:
\begin{gather}
	\oint \textbf{U} \cdot \textbf{n} \ dS = 0.
	\label{eqcontinuidadeincompressivel}
\end{gather}

Esta última equação pode ser obtida partindo do conhecimento de que um fluido incompressível possui massa específica constante, logo $\rho$ pode ser retirada de dentro da integral da Eq. \eqref{eqcontinuidadecompressivel}, consequentemente o termo à esquerda é nulo, já que não há variação da quantidade de massa dentro do volume de controle. Dividindo toda Eq. \eqref{eqcontinuidadecompressivel} por $\rho$ chega-se a Eq. \eqref{eqcontinuidadeincompressivel}. Esta integral de superfície confirma o balanço dos fluxos nas faces. Nesta dedução assumiu-se um volume de controle cúbico, e portanto com seis superfícies, porém,

\begin{citacao}
``dependendo da forma do volume, deve-se avaliar mais ou menos faces. A formulação integral da equação da continuidade leva ao chamado Método dos Volumes Finitos (MVF). Este método é conservativo e nós podemos aplicá-lo para qualquer volume arbitrário (hexaedros, tetraedros, prismas, cunhas e assim por diante) o que faz do método tão popular.'' \cite{bib:tobias} \footnote{Texto original:``Depending on the shape of the volume, we have to evaluate more or less faces. The integral form of the continuity equation leads to the so called finite volume method (FVM). This method is conservative and we can apply this method to each arbitrary volume (hexaeder, tetraeder, prisms, wedges and so on) which makes this method popular.''}.
\end{citacao}


\subsection{Equação da Conservação do Momento}\label{sec:momento}

Nesta subseção a palavra \textit{momento} é utilizada como sinônimo de \textit{momento linear}. E para a dedução da sua equação de conservação continuará sendo utilizado um elemento de volume $dV$ cúbico e as mesmas notações. Porém, aqui há duas diferenças a serem consideradas em relação a conservação da massa: primeiro, que agora deve-se considerar mais fenômenos que podem transportar e alterar o momento dentro do elemento de volume e, segundo, que a grandeza momento não é escalar, mas sim, vetorial. 

De modo geral, pode-se dizer que o momento pode ser transportado e alterado pelos seguintes aspectos:  
\begin{gather}
\hspace{0.5cm} \left[\begin{array}{c}
\text{taxa de acúmulo de}\\
\text{momento dentro de d}V
\end{array}\right]=	
\left[\begin{array}{c}
\text{taxa de momento}\\
\text{entrando em d}V
\end{array}\right] -
\left[\begin{array}{c}
\text{taxa de momento}\\
\text{saindo de d}V
\end{array}\right] \nonumber \\ \hspace{5.7cm}+ 
\left[\begin{array}{c}
\text{soma das forças}\\
\text{que agem em d}V
\end{array}\right]. 
\label{momentoarray}  
\end{gather}

A componente $x$ do momento, assim como a massa, é transportada por convecção para dentro do elemento de volume. Esta convecção se dá através de todas as seis faces e pode ser deduzida de forma similar ao transporte convectivo de massa. O momento na direção $x$ entra no volume através da face $|_{x}$ e sai pela face $|_{x + \Delta x}$, da mesma forma que a equação da continuidade (ver Fig. \ref{fig:dv}), entretanto, agora também é possível que a componente $x$ do momento seja transportada pelas faces nas direções $y$ e $z$.

O transporte do momento devido à convecção, então, pode ser escrito como o produto da velocidade na direção $x$ ($u_{x}$) pelo fluxo através da face que estiver sendo analisada:

\begin{itemize}

\item Entrando em $|_{x}$: \  \ $(\rho u_{x}) u_{x}|_{x}$, \vspace{-0.25cm}
\item Saindo em $|_{x + \Delta x}$: \ $(\rho u_{x}) u_{x}|_{x + \Delta x}$, 
\item Entrando em $|_{y}$: \ \ $(\rho u_{y}) u_{x}|_{y}$, \vspace{-0.25cm}
\item Saindo em $|_{y + \Delta y}$: \ $(\rho u_{y}) u_{x}|_{y + \Delta y}$, 
\item Entrando em $|_{z}$: \ \ $(\rho u_{z}) u_{x}|_{z}$, \vspace{-0.25cm}
\item Saindo em $|_{z + \Delta z}$: \ $(\rho u_{z}) u_{x}|_{z + \Delta z}$.

\end{itemize} 

Combinando os termos e usando a área das faces, obtêm-se uma expressão para o transporte do momento relacionado à convecção:
\begin{gather}
\hspace{0.3cm}\big((\rho u_{x}) u_{x}|_{x} - (\rho u_{x}) u_x|_{x + \Delta x} \big) \Delta y \Delta z \nonumber \\
+ \big((\rho u_{y}) u_{x}|_{y} - (\rho u_{y}) u_x|_{y + \Delta y} \big) \Delta x \Delta z \label{transportemomentoconvectivo} \\
+ \big((\rho u_{z}) u_{x}|_{z} - (\rho u_{z}) u_x|_{z + \Delta z} \big) \Delta x \Delta y. \nonumber
\end{gather}

No caso do transporte de massa, a convecção era o único fenômeno de transporte envolvido. Já o transporte de momento, além da convecção, pode dar-se também através de efeitos moleculares. Na Fig. \ref{fig:dv2}, é mostrado um elemento de volume semelhante àquele da Fig. \ref{fig:dv}, onde se verifica a presença do fenômeno de transporte molecular referido, que age nas faces do cubo. 

\begin{figure}[htb!]{11.5cm}
	\caption{Elemento de volume $dV$ e fluxo de momento através de suas faces}
	\includegraphics[scale=0.6]{dvmomento}
	\source{Os autores, 2017.}
	\label{fig:dv2}
\end{figure}

O fenômeno mostrado na Fig. \ref{fig:dv2} é a tensão e seu efeito é baseado em gradientes de velocidade. A tensão, representada pela letra grega $\tau$, é uma forma de transporte de momento baseada em efeitos moleculares que pode agir normal e tangencialmente à superfície. Costumeiramente, são utilizados índices duplos para designar as tensões ($\tau_{ij}$), sendo que $i$ indica o plano (perpendicular a $i$) no qual a tensão atua e $j$ indica a direção em que ela atua. 

As tensões normais são perpendiculares às faces que estiverem sendo analisadas, consequentemente seus dois índices serão iguais, pois a direção em que a tensão atua coincidirá com o plano perpendicular no qual ela age ($\tau_{xx}$ é uma tensão normal na Fig. \ref{fig:dv2}). As tensões tangenciais ou cisalhantes, como o próprio nome diz, são tangenciais ao plano em que atuam, logo seus índices serão sempre diferentes ($\tau_{yx}$ e $\tau_{zx}$ são tensões cisalhantes na Fig. \ref{fig:dv2}). Estas tensões são chamadas de cisalhantes por serem geradas pelos gradientes de velocidade que causam o fenômeno do cisalhamento.

Os termos de transporte da componente $x$ do momento (nas faces do elemento de volume) devido aos efeitos moleculares podem, então, serem escritos da seguinte forma:
\begin{itemize}	
\item Entrando em $|_{x}$: \ \ $\tau_{xx}|_{x} \ \Delta y \Delta z$, \vspace{-0.25cm}
\item Saindo em $|_{x + \Delta x}$: \ $\tau_{xx}|_{x+\Delta x} \ \Delta y \Delta z$, 

\item Entrando em $|_{y}$: \ \ $\tau_{yx}|_{y} \ \Delta x \Delta z$, \vspace{-0.25cm}
\item Saindo em $|_{y + \Delta y}$: \ $\tau_{yx}|_{y+\Delta y} \ \Delta x \Delta z$, 

\item Entrando em $|_{z}$: \ \ $\tau_{zx}|_{z} \ \Delta x \Delta y$, \vspace{-0.25cm}
\item Saindo em $|_{z + \Delta z}$: \ $\tau_{zx}|_{z+\Delta z} \ \Delta x \Delta y$. 
\end{itemize}
Assim, o transporte molecular do momento em $x$ através das superfícies pode ser escrito como
\begin{gather}
\hspace{0.3cm}(\tau_{xx}|_{x} - \tau_{xx}|_{x+\Delta x}) \Delta y \Delta z
+(\tau_{yx}|_{y} - \tau_{yx}|_{y+\Delta y}) \Delta x \Delta z 
+(\tau_{zx}|_{z} - \tau_{zx}|_{z+\Delta z}) \Delta x \Delta y. \label{transportemomentomolecular}
\end{gather}

Quanto ao último termo na Eq. \eqref{momentoarray}, na maioria dos casos, as únicas forças relevantes que influenciam o momento são a \textit{pressão} ($p$) e a \textit{gravidade} ($\bf{g}$). A pressão é uma força de superfície, pois age apenas sobre as superfícies do elemento de fluido, enquanto a gravidade é uma força de campo, que age sobre toda a massa de fluido. Dessa forma, o efeito na componente $x$ do momento devido à forças de superfície e de campo pode ser escrita da seguinte forma:
\begin{equation}
(p|_{x} - p|_{x + \Delta x}) \Delta y \Delta z + \rho g_{x} \Delta x \Delta y \Delta z,
\label{transportemomentoforcas}
\end{equation}
em que $p$ é a pressão estática e $g_{x}$ é a componente do vetor gravitacional na direção $x$. Como sabido, o acúmulo de momento dentro de um elemento de volume arbitrário é dado por:
\begin{equation*}
\dfrac{\Delta}{\Delta t} (\rho u_{x}) \Delta x \Delta y \Delta z,
\label{acumulo}
\end{equation*}
então, utilizando a equação acima, e as Eqs. \eqref{transportemomentoconvectivo}, \eqref{transportemomentomolecular} e \eqref{transportemomentoforcas} pode-se reescrever a Eq. \eqref{momentoarray} com expressões matemáticas. Na direção $x$ tem-se que:
\begin{empheq}[right= \quad \empheqrbrace \text{Efeitos convectivos}]{gather}
\dfrac{\Delta}{\Delta t} (\rho u_{x}) \Delta x \Delta y \Delta z = \big((\rho u_{x}) u_{x}|_{x} - (\rho u_{x}) u_x|_{x + \Delta x} \big) \Delta y \Delta z \nonumber \\	
\hspace{3.5cm} + \big((\rho u_{y}) u_{x}|_{y} - (\rho u_{y}) u_x|_{y + \Delta y} \big) \Delta x \Delta z \nonumber \\
\hspace{3.5cm} + \big((\rho u_{z}) u_{x}|_{z} - (\rho u_{z}) u_x|_{z + \Delta z} \big) \Delta x \Delta y \nonumber
\end{empheq} \vspace{-0.5cm}
\begin{empheq}[right= \quad \empheqrbrace \text{Efeitos moleculares}]{gather}
\hspace{3.5cm} + (\tau_{xx}|_{x} - \tau_{xx}|_{x+\Delta x}) \Delta y \Delta z \label{momentodirecaox}\\
\hspace{3.5cm} + (\tau_{yx}|_{y} - \tau_{yx}|_{y+\Delta y}) \Delta x \Delta z \nonumber \\
\hspace{3.5cm} + (\tau_{zx}|_{z} - \tau_{zx}|_{z+\Delta z}) \Delta x \Delta y \nonumber
\end{empheq}
\begin{empheq}[right= \quad \empheqrbrace \text{Efeitos de forças de superfície e campo}]{gather}
\hspace{3.6cm} + (p|_{x} - p|_{x + \Delta x}) \Delta y \Delta z \nonumber \\
\hspace{3.6cm} + \rho g_{x} \Delta x \Delta y \Delta z. \nonumber
\end{empheq}\\

Agora, dividindo toda esta equação pelo volume $dV$, bem como assumindo um elemento de volume infinitesimal (ver Eq. \eqref{infinitesimalx})  e um intervalo de tempo também infinitesimal (ver Eq. \eqref{infinitesimalt}) é possível reescrever a equação do momento para a direção $x$ da seguinte forma:
\begin{gather}
\frac{\partial}{\partial t} (\rho u_{x}) = -\left(\frac{\partial}{\partial x} \rho u_{x} u_{x} + \frac{\partial}{\partial y} \rho u_{y} u_{x} + \frac{\partial}{\partial z} \rho u_{z} u_{x} \right)  \nonumber \\
\hspace{2cm} - \left(\frac{\partial}{\partial x} \tau_{xx} + \frac{\partial}{\partial y} \tau_{yx} + \frac{\partial}{\partial z} \tau_{zx} \right) + \frac{\partial p}{\partial x} + \rho g_{x}. \label{momentodirecaoxfinal}
\end{gather}

Adotando os mesmos procedimentos para as componentes $y$ e $z$ podem ser obtidas as equações para o momento nestas respectivas direções.

Para componente $y$:
			\begin{gather}
			\frac{\partial}{\partial t} (\rho u_{y}) = -\left(\frac{\partial}{\partial x} \rho u_{x} u_{y} + \frac{\partial}{\partial y} \rho u_{y} u_{y} + \frac{\partial}{\partial z} \rho u_{z} u_{y} \right)  \nonumber \\
			\hspace{2cm} - \left(\frac{\partial}{\partial x} \tau_{xy} + \frac{\partial}{\partial y} \tau_{yy} + \frac{\partial}{\partial z} \tau_{zy} \right) + \frac{\partial p}{\partial y} + \rho g_{y}; \label{momentodirecaoyfinal}
			\end{gather}	

Para componente $z$:
			\begin{gather}
			\frac{\partial}{\partial t} (\rho u_{z}) = -\left(\frac{\partial}{\partial x} \rho u_{x} u_{z} + \frac{\partial}{\partial y} \rho u_{y} u_{z} + \frac{\partial}{\partial z} \rho u_{z} u_{z} \right)  \nonumber \\
			\hspace{2cm} - \left(\frac{\partial}{\partial x} \tau_{xz} + \frac{\partial}{\partial y} \tau_{yz} + \frac{\partial}{\partial z} \tau_{zz} \right) + \frac{\partial p}{\partial z} + \rho g_{z}. \label{momentodirecaozfinal}
			\end{gather}	


Estritamente falando, as Eqs. \eqref{momentodirecaoxfinal}, \eqref{momentodirecaoyfinal} e \eqref{momentodirecaozfinal} do momento linear são as propriamente ditas ``equações de Navier-Stokes'', mas é muito comum utilizar essa expressão para se referir ao conjunto das equações do momento, da continuidade e da energia (se presente) \cite{bib:fortuna}. Ainda sobre as equações do momento, é possível unificá-las, e para isto introduz-se a seguir as formulações matemáticas do vetor aceleração da gravidade, do gradiente de pressão e do tensor de tensões:

\begin{itemize}
	\item Gradiente de pressão: \hspace{1cm}
			$ \nabla p = \begin{pmatrix}
				\frac{\partial p}{\partial x} \\
				\frac{\partial p}{\partial y} \\
				\frac{\partial p}{\partial z} 
			\end{pmatrix}$;
			
	\item Tensor de tensões: \hspace{1cm}
			$\bm{\tau} = \begin{pmatrix}
				\tau_{xx} & \tau_{xy} & \tau_{xz} \\
				\tau_{yx} & \tau_{yy} & \tau_{yz} \\
				\tau_{zx} & \tau_{zy} & \tau_{zz} 
			\end{pmatrix}$;
	\item Vetor aceleração gravitacional: \hspace{1cm} $\bm{g} = \begin{pmatrix}
				g_{x} \\
				g_{y} \\
				g_{z}
			\end{pmatrix}$.
\end{itemize} 

Finalmente, é possível obter-se uma única equação vetorial que descreva a conservação do momento,
\begin{equation}
	\frac{\partial }{\partial t} \rho \textbf{U} = - \nabla \bullet (\rho \textbf{U} \otimes \textbf{U}) - \nabla \bullet \bm{\tau} - \nabla p + \rho \textbf{g}.
	\label{eqconservacaomomentovetorial}
\end{equation} 

Para a obtenção da forma integral da Eq. \eqref{eqconservacaomomentovetorial}, similarmente à equação da continuidade, aplica-se o teorema de Gauss (ver Eq. \eqref{teoremadegauss}), transformando os termos com divergência em termos com integrais de superfícies, descrevendo assim o fluxo através destas superfícies,
\begin{equation*}
	\frac{\partial}{\partial t} \int \rho \textbf{U} \ dV = - \oint (\rho \textbf{U} \otimes \textbf{U}) \cdot \textbf{n} \ dS - \oint \bm{\tau} \cdot \textbf{n} \ dS - \int p \ dV + \int \rho \textbf{g} \ dV.
	\label{eqconservacaomomentointegral}
\end{equation*}

%\subsection{Equação da Energia Total}\label{sec:energia}
%Nesta subseção será abordada brevemente a formulação da equação da energia. A energia total de um sistema inclui as energias interna (relacionada a fenômenos térmicos) e cinética (de natureza mecânica). De modo geral, a variação da energia total num elemento de volume arbitrário $dV$ pode ser descrita por:
%
%\begin{gather}
%\left[\begin{array}{c}
%\text{taxa de acúmulo}\\
%\text{de energia} \\
%\text{interna e cinética}
%\end{array}\right]=	
%\left[\begin{array}{c}
%\text{taxa de energia}\\
%\text{interna e cinética}\\
%\text{entrando no volume}
%\end{array}\right] -
%\left[\begin{array}{c}
%\text{taxa de energia}\\
%\text{interna e cinética}\\
%\text{deixando o volume}
%\end{array}\right] \nonumber \\ \hspace{7.1cm} +
%\left[\begin{array}{c}
%\text{taxa líquida de}\\
%\text{fontes de}\\
%\text{calor adicionais}
%\end{array}\right] \label{energiaarray} \\ \hspace{4.21cm} + 
%\left[\begin{array}{c}
%\text{taxa líquida de}\\
%\text{calor adicionado}\\
%\text{por condução}
%\end{array}\right] -
%\left[\begin{array}{c}
%\text{taxa líquida de trabalho}\\
%\text{realizado pelo}\\
%\text{sistema sobre o ambiente}
%\end{array}\right]. \nonumber  
%\end{gather}
%
%Esta equação é a primeira lei da termodinâmica escrita para um sistema de estado aberto e instável, considerando fontes de calor adicionais \apud{bib:bird}{bib:tobias}. Assumir um sistema aberto significa dizer que tanto massa quanto energia podem cruzar as superfícies do volume de controle $dV$. Perceba que diferentemente das subseções \ref{sec:continuidade} e \ref{sec:momento}, leva-se em conta agora os fenômenos de escala microscópica, que estão incluídos nos termos de energia interna (por unidade de volume).
%
%
%\textit{Energia interna} ($e$) é uma forma \textit{microscópica} de energia que está relacionada à estrutura molecular de um sistema e ao grau de atividade molecular (e consequentemente à temperatura). Pode ser definida como a soma de todas as formas microscópicas de energia de um sistema, que geralmente se resumem a energia cinética e potencial das moléculas. Energia interna é independente de referências externas por ser \textit{contida} ou \textit{armazenada} em um sistema, e por isso que pode ser vista como uma forma \textit{estática} de energia. Por outro lado, tem-se as formas \textit{dinâmicas} de energia que também são vistas como interações, formas de energia como esta serão percebidas quando atravessarem as superfícies do elemento $dV$, e representam a energia ganha ou perdida por ele durante o escoamento  \cite{bib:cengel}. Na Eq. \eqref{energiaarray} estas formas estão representadas pela transferência de \textit{calor} e pelo \textit{trabalho} realizado.
%
%
%Quanto a parte macroscópica da expressão, tem-se a \textit{energia cinética}, que está relacionada ao movimento e à influência de alguns efeitos externos como gravidade, magnetismo, eletricidade e tensão superficial. Energia cinética pode ser definida como a energia que um sistema possui como resultado de seu movimento em relação a alguma estrutura de referência, e pode ser expressa --- por unidade de volume --- como $\frac{1}{2} \rho |\bm{U}|^{2}$ em que $|\bm{U}|$ representa a magnitude da velocidade local.
%
%Omitidas todas as demonstrações, pode-se escrever a equação da conservação da energia total na sua forma vetorial como:
%\begin{gather}
%\frac{\partial }{\partial t} (\rho e + \frac{1}{2} \rho |\textbf{U}|^2) = \underbrace{- \nabla \bullet (\rho \textbf{U} (e+\frac{1}{2} |\textbf{U}|^2))}_{\text{convecção}} \underbrace{- \nabla \bullet \bm{q}}_{\text{condução}} + \underbrace{\rho (\textbf{U} \bullet \bm{g})}_{\text{gravidade}} \nonumber \\
%\hspace{3.5cm} \underbrace{- \nabla \bullet (p \textbf{U})}_{\text{pressão}} \underbrace{- \nabla \bullet [\bm{\tau} \bullet \textbf{U}]}_{\text{forças viscosas}} + \underbrace{\rho S_{\phi}}_{\text{fontes de calor}}.
%\label{eqconservacaoenergiavetorial}
%\end{gather}
%
%Os símbolos desconhecidos são $\bm{q}$, que representa o \textit{vetor fluxo de calor}, e $S_{\phi}$ \footnote{Do inglês ``Source'' e ``Sink'', em português \textit{Fonte} e \textit{Sumidouro.}} associado ao termo fonte nas equações. O trabalho realizado pelo elemento de fluido sobre a sua vizinhança está dividido em três termos, o da gravidade, da pressão e das forças viscosas. O motivo de não deduzir-se a equação da energia é porque a mesma não está presente na metodologia do trabalho. Isto porque o presente problema não envolve variação de temperatura ou de massa específica, ou seja, é um caso \textit{isotérmico} e \textit{incompressível}. A equação (\ref{eqconservacaoenergiavetorial}) é mostrada aqui para fins didáticos, deduções podem ser encontradas em \citeonline{bib:versteeg} e \citeonline{bib:tobias}.

\subsection{Equação Geral de Transporte: forma Diferencial}

A partir da equação geral de transporte é possível obter todas as equações de conservação tratadas nas seções anteriores. Para uma formulação geral, assume-se uma variável genérica denotada aqui como $\phi$ e o elemento de fluido $dV$, assim, a forma conservativa das equações de fluxo de \textit{fluido} para esta propriedade arbitrária pode ser dada por:
\begin{gather}
\underbrace{\frac{\partial}{\partial t} (\rho \phi)}_{\text{acumulação temporal}} = \underbrace{- \nabla \bullet (\rho \textbf{U} \phi)}_{\text{fluxo convectivo}} + \underbrace{\nabla \bullet (\Gamma \nabla \phi)}_{\text{fluxo difusivo}} + \underbrace{S_{\phi}}_{\text{termo fonte}},
\label{eqgeraldetransortederivativa}
\end{gather}
em que $\Gamma$ é o coeficiente de difusão, que pode ser um escalar ou um vetor e $S_{\phi}$ representa qualquer tipo de fontes ou sumidouros que influenciam a quantidade $\phi$. A \textit{acumulação temporal} é vista como a taxa de aumento de $\phi$ no elemento de fluido, o \textit{fluxo convectivo} representa a taxa líquida do fluxo de $\phi$ para fora do elemento de fluido, já o \textit{fluxo difusivo} seria a taxa de aumento de $\phi$ devido a difusão e, por fim, o \textit{termo fonte} deve ser entendido como a taxa de aumento de $\phi$ devido a atuação de fontes \cite{bib:versteeg}. Os fluxos da Eq. \eqref{eqgeraldetransortederivativa} são explicados com interpretação física por \citeonline{bib:fortuna}:

\begin{itemize}
	\item \textit{Fluxos convectivos}: ocorrem devido à velocidade do fluido. Estes tipos de fluxo possuem a forma geral $\rho \phi \bf{U}$, em que $\rho$ é um termo de massa específica, $\bf{U}$ o vetor velocidade do fluido e $\phi$ a propriedade arbitrária em questão. Considerando uma direção qualquer, o fluxo convectivo é dado por $\rho \phi \textbf{U} \cdot \bf{n}$, sendo $bf{n}$ o vetor unitário normal à área nessa direção.
	
	\item \textit{Fluxos difusivos}: são oriundos pela não uniformidade da distribuição de $\phi$ (perceba que o termo difusivo inclui o gradiente desta propriedade), isto quer dizer que o transporte difusivo pode ocorrer independentemente da existência do escoamento. Os fluxos difusivos assumem a forma geral $\Gamma \nabla \phi$, sendo que $\Gamma$ não é necessariamente uniforme e a propriedade $\phi$ é difundida com o transporte. Se $\bf{n}$ for o vetor unitário ao longo de uma direção arbitrária, tem-se que $\Gamma \nabla \phi \cdot \bf{n}$ é o fluxo difusivo nesta direção.
\end{itemize}

A Equação \eqref{eqgeraldetransortederivativa} é utilizada como ponto de partida para procedimentos computacionais do Método dos Volumes Finitos. O passo chave para o MVF é a integração desta equação sobre um volume de controle, que será assunto para a próxima seção.




