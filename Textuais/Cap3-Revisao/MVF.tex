\section{Método dos Volumes Finitos} \label{secao:mvf}
Método dos Volumes Finitos (MVF)\footnote{\textit{Finite Volume Method} (FVM), em inglês.} é um método numérico através do qual discretiza-se as equações diferenciais parciais que representam a conservação de uma grandeza, discutidas nas seções anteriores. O MVF tem uma formulação baseada em equações discretas que satisfazem os princípios de conservação \cite{bib:jatoba}. A partir desta formulação é possível a implementação computacional das equações de balanço e subsequentemente, a resolução destas.
De maneira geral, o Método dos Volumes Finitos envolve as seguintes etapas:
\begin{enumerate}[label={(\arabic*)}]
	\item Decomposição do espaço físico do problema em volumes de controle (discretização do domínio);
	\item Aplicar a formulação integral das equações de balanço para cada volume de controle (discretização das equações);
	\item Aproximar o valor das integrais utilizando uma técnica de integração numérica;
	\item Aproximar os valores das funções e suas derivadas nas faces dos volumes pela interpolação dos valores nodais (no ponto central);
	\item Solução do sistema algébrico das equações discretizadas.
\end{enumerate}

A popularidade deste método na Dinâmica dos Fluidos Computacional se dá em função da sua flexibilidade como método de discretização. Nele, a discretização ocorre diretamente no espaço físico, sem necessidade de qualquer transformação entre o físico e o sistema de coordenadas computacional \cite{bib:moukalled}. Ou seja, a discretização das equações é aplicada diretamente sobre a malha gerada na discretização do domínio.

Outro aspecto importante do MVF é que os termos discretizados permanecem com significado físico e refletem os princípios de conservação das equações governantes, tal como a propriedade integral (ou conservativa). Um arranjo matemático como este torna possível a resolução de fluxos em geometrias complexas sem afetar a simplicidade da formulação. Este é um dos motivos pelos quais o Método dos Volumes Finitos abrange uma ampla gama de aplicações e é bastante utilizado em problemas de fluxo \cite{bib:moukalled}.

A formulação matemática do método pode ser obtida a partir da Equação Geral de Transporte (Eq. \eqref{eqgeraldetransortederivativa}), aplicando sobre ela a integração em um volume d$V$:
\begin{equation}
	\int_{dV} \frac{\partial}{\partial t} (\rho \phi) \ dV + \int_{dV} \nabla \bullet (\rho \phi \textbf{U}) \ dV = \int_{dV} \nabla \bullet (\Gamma \ \nabla \phi) \ dV + \int_{dV} S_{\phi} \ dV.
\end{equation}

Os termos que envolvem o operador divergente (convectivo e difusivo) podem ser reescritos pelo teorema de Gauss (Eq. \eqref{teoremadegauss}). Logo,
\begin{equation}
	\frac{\partial}{\partial t}  \left (\int_{dV} \rho \phi \ dV \right ) + \int_{S} (\rho \phi \textbf{U}) \cdot \textbf{n} \ dS = \int_{S} \left(\Gamma \ \nabla \phi \right ) \cdot \textbf{n} \ dS + \int_{dV} S_{\phi} \ dV.
	\label{integracaoeqgeral} 
\end{equation}

A ordem da integração e diferenciação no primeiro termo à esquerda da Eq. \eqref{integracaoeqgeral} é alterada para representar seu significado físico: a variação da quantidade total da propriedade $\phi$ no volume de controle (o acúmulo). O segundo termo representa a integração do fluxo convectivo ($\rho \phi \bf{U}$) na direção $\bf{n}$ que é normal à superfície $S$, ou seja, do fluxo convectivo deixando o elemento. A integração do termo difusivo ($\Gamma \ \nabla \phi$) diz respeito ao fluxo difusivo entrando no volume de controle, sendo este positivo na direção de um gradiente negativo da propriedade $\phi$ \cite{bib:versteeg}.

Tratando-se de problemas estacionários, o termo transiente da Eq. \eqref{integracaoeqgeral} é nulo. Assim, a Equação de Transporte Estacionária é dada por:
\begin{equation}
\int_{S} (\rho \phi \textbf{U}) \cdot \textbf{n} \ dS = \int_{S} \left(\Gamma \ \nabla \phi \right ) \cdot \textbf{n} \ dS + \int_{dV} S_{\phi} \ dV.
\label{eqtransporteestacionaria}
\end{equation}

\section{Discretização}\label{sec:discretizacao}
Para o caso bidimensional, a Fig. \ref{celulas} ilustra a discretização do domínio espacial por células quadrangulares. Neste caso, o volume de controle $dV$ de ponto central \textit{P} possui as faces \textit{f: n}, \textit{e}, \textit{s} e \textit{w}. Suas células vizinhas possuem os pontos centrais \textit{N}, \textit{E}, \textit{S} e \textit{W}, seguindo a nomenclatura padrão do MVF que é inspirada pelos pontos cardeais\footnote{\textit{North}, \textit{East}, \textit{South} e \textit{West}, em inglês.} \cite{bib:versteeg}.

\begin{figure}{8cm}
	\caption{Células de uma malha quadrangular}
	\label{celulas}
	\includegraphics[scale=0.5]{celulas.png}
	\source{Os autores, 2017.}
\end{figure}   

Como já dito, a discretização das equações de Navier-Stokes é feita a partir da integração sobre cada célula da malha. No caso da malha da Fig. \ref{celulas}, temos que a superfície \textit{S} de integração da Eq. \eqref{eqtransporteestacionaria} corresponde as faces \textit{f}, e os volumes $dV$ às células quadrangulares \footnote{Para manter a terminologia usual do MVF uma célula da malha é sempre chamada de \textit{volume de controle}, embora, tecnicamente a nomenclatura só esteja correta para o caso tridimensional.}. Portanto, a solução numérica da equação geral de transporte estacionária aplicada para uma célula da malha é dada por:
\begin{equation}
\sum_{f} \underbrace{\int_{S} (\rho \phi \textbf{U}) \cdot \textbf{n} \ dS}_{\text{fluxo convectivo na face}} = \sum_{f} \underbrace{\int_{S} \left(\Gamma \ \nabla \phi \right ) \cdot \textbf{n} \ dS}_{\text{fluxo difusivo na face}} + \underbrace{\int_{dV} S_{\phi} \ dV}_{\text{termo fonte}}.
\label{eqgeralsemidiscretizada} 
\end{equation}

As integrais dos fluxos convectivo e difusivos, que são normais às superfícies $f$ do volume de controle, podem ser aproximadas por uma técnica de integração numérica apropriada \cite{bib:jatoba}.

A Equação \eqref{eqgeralsemidiscretizada} pode ser reescrita na forma:
\begin{equation}
	\sum_{f} F_{f}^{c} - \sum_{f} F_{f}^{d} = \int_{dV} S_{\phi} \ dV,
\end{equation}
em que $F_{f}^{c}$ corresponde à aproximação do fluxo convectivo e $F_{f}^{d}$ do fluxo difusivo em uma face $f$ de qualquer célula $dV$ da malha. De acordo com \citeonline{bib:jatoba}, estas aproximações são a etapa de maior importância para o MVF, sua tese cita as principais técnicas para cada termo, discutiremos algumas a seguir.

\subsection{Fluxo Convectivo}
A aproximação da integral do fluxo convectivo na superfície do volume de controle pode ser descrita por
\begin{equation}
	F_{f}^{c} = \int_{S} (\rho \phi \textbf{U}) \cdot \textbf{n} \ dS \approx \sum_{f} (\phi)_{f} (\rho \textbf{U} \cdot \textbf{n})_{f}  \ A_{f},
	\label{fluxoadvectivosemidiscretizado} 
\end{equation}
em que $A_{f}$ diz respeito a área da face $f$. A Equação \eqref{fluxoadvectivosemidiscretizado} diz que as integrais de superfície são aproximadas pelos seus respectivos valores nas faces, $(\phi)_{f}$. Para isto, o valor da quantidade $\phi$ em uma face $f$ é interpolado a partir do ponto central \textit{P} de $dV$ por meio de uma função de interpolação. A escolha do esquema de interpolação é um passo importante, pois dependendo das condições do problema o esquema pode falhar e os fluxos podem ser calculados erroneamente. A função de interpolação mais simples, segundo \citeonline{bib:jatoba}, é a aproximação pelo esquema de diferenças centrais (CDS)\footnote{\textit{Central Differencing Scheme} (CDS), em inglês.}. Em problemas puramente convectivos, o uso do CDS --- assim como de outros esquemas de alta ordem --- acarreta soluções não realísticas, uma vez que é um esquema não dissipativo. Já os esquemas \textit{upwind} (UDS)\footnote{\textit{Upwind Differencing Scheme}.}  produzem soluções físicas coerentes e possuem estabilidade numérica, mas por serem dissipativos tendem a suavizar os altos gradientes \cite{bib:maliska}.   

\subsection{Fluxo Difusivo}

De forma similar ao fluxo convectivo, a aproximação da integração do fluxo difusivo pode ser dada por
\begin{equation}
	F_{f}^{d} = \int_{S} \left(\Gamma \ \nabla \phi \right ) \cdot \textbf{n} \ dS \approx \sum_{f} \left(\Gamma \cdot \ \bf{n} \nabla \phi \right )_{f}  \ A_{f}.
\label{fluxodifusivoemidiscretizado}
\end{equation}

Diversas funções interpoladoras podem ser utilizadas para a aproximação do termo difusivo nas faces, dentre elas \citeonline{bib:jatoba} destaca o CDS e até mesmo o método dos mínimos quadrados.


\subsection{Termo Fonte} 
Para este termo, a aproximação das integrais no volume $dV$ para o tratamento do termo fonte é realizada considerando a aproximação para o valor da função no centro do volume de controle \cite{bib:jatoba}:
\begin{equation}
	\int_{dV} S_{\phi} \ dV \approx (\phi)_{P} \ dV
	\label{termofontesemidiscretizado}
\end{equation}

\subsection{Equação Discretizada}
Como vimos, a formulação do MVF leva em conta aproximações para os fluxos, de modo que o cálculo da propriedade $\phi$ para um volume de controle leva em conta os valores de $\phi$ nas faces dos volumes de controle vizinhos. Partindo deste contexto, uma expressão geral pode ser obtida para um volume de controle $dV$, 
\begin{equation}
	a_{P} \phi_{P}= a_{N} \phi_{N} + a_{E} \phi_{E} + a_{S} \phi_{S} + a_{W} \phi_{W} + b_{P}
	\label{formageraldiscretizada}
\end{equation}
que pode ser simplificada na forma,
\begin{equation}
	a_{P} \phi_{P} + \displaystyle{\sum_{NB} a_{NB} \phi_{NB}} = b_{P},
	\label{formacompactadiscretizada}
\end{equation}
em que $\phi$ é a propriedade a ser calculada, \textit{P} diz respeito ao ponto central da célula e \textit{NB} se refere a todas células vizinhas. Os coeficientes $a_{P}$, $a_{NB}$ e $b_{P}$ são obtidos a partir da discretização da equação de conservação em questão, e admitem expressões de acordo com cada problema específico.
O produto final do MVF é um sistema algébrico de equações, conseguido a partir do uso das aproximações aqui discutidas para todos os volumes da malha e levando em conta as condições de contorno coerentes com o problema estudado. 






