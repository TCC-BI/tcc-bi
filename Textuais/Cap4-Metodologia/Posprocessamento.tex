\section{Pós-Processamento} \label{secao:posprocessamento}

A principal ferramenta de pós-processamento do OpenFOAM\textregistered \  é o utilitário \textit{paraFoam}, um módulo de leitura para executar o \textit{ParaView}. O \textit{Paraview} é um \textit{software} de visualização e possui código livre e interface gráfica. O \textit{paraFoam} faz uso de duas bibliotecas: \textit{PV4FoamReader} e \textit{vtkPV4Foam}. Como o ParaView utiliza a \textit{Visualisation Toolkit} (VTK) para o processamento e renderização de dados, qualquer dado no formato \textit{VTK} pode ser lido por ele. O OpenFOAM\textregistered \ inclui um utilitário chamado \textit{foamToVTK} que converte dados nativos para o formato \textit{VTK}, dessa maneira qualquer ferramenta gráfica baseada no formato \textit{VTK} pode ser utilizada para gerenciamento de dados OpenFOAM\textregistered \ \cite{bib:greenshields}.

Para utilizar o Paraview basta executar o comando ``\textit{paraFoam}'' via terminal no diretório do caso problema. Se ``\textit{case}'' for a nomenclatura do diretório, automaticamente será criado um arquivo ``\textit{case.foam}'' que abrirá no ParaView. Os parâmetros que podem ser extraídos dos resultados são configurados na aba \textit{Properties}, como ilustra a Fig. \ref{paraview1}.
\begin{figure}[htb!]{15cm}
\caption{Aba Propriedades do Paraview}
\includegraphics[scale=0.6]{propertiesparaview}
\source{Os autores, 2017.}
\label{paraview1}
\end{figure}
Apenas os grupos físicos (criados no \textit{Gmsh}) marcados na opção \textit{Mesh Regions} serão mostrados na interface. Já os campos, em geral de pressão e velocidade, são escolhidos em \textit{Cell Arrays}. A seção \textit{Coloring} trata da representação gráfica dos dados desses campos, esta ferramenta atribui automaticamente uma função de transferência de cores à um campo ou variável que deseja-se representar, seja por meio de uma componente específica ou pela magnitude. De forme que a matriz de parâmetros obtida na simulação seja mapeada com cores representando valores. O intervalo padrão de valores do ParaView para esta função pode ser adaptado para os limites do caso problema acionando o botão ``\textit{Rescale}''. A opção ``\textit{Edit}'' abre uma aba que fornece a possibilidade de se alterar manualmente este intervalo, assim também como a legenda da representação e a paleta de cores (ver a seção 11.2 de \citeonline{bib:paraview}).

Outra ferramenta do ParaView importante para este trabalho é a \textit{StreamTracer}, um filtro utilizado para gerar as linhas de corrente para campos vetoriais. As linhas de corrente, na visualização, se referem a curvas que são instantaneamente tangenciais ao campo vetorial no conjunto de dados. Elas fornecem uma indicação da direção em que as partículas viajariam no exato instante de tempo ao qual estão relacionadas. Para criar as \textit{streamlines} o ParaView toma um conjunto de pontos nos dados, chamados de pontos de semente, e a partir deles integra as linhas de corrente \cite{bib:paraview}. A opção pode ser encontrada no Menu \textit{Filters} e o seu layout é ilustrado na Fig. \ref{streamtracer}.
\begin{figure}[htb!]{10cm}
	\caption{Proprieades da \textit{StreamTracer}}
	\includegraphics[scale=0.6]{streamtracer}
	\source{Os autores, 2017.}
	\label{streamtracer}
\end{figure}
O \textit{Seed Type} exemplificado na Fig. \ref{streamtracer} é o \textit{Point Source}, nele as linhas de corrente são criadas a partir de uma nuvem de pontos definida em torno de um ponto central. Para isto, os seguintes parâmetros devem ser configurados: a direção e o método de integração, o máximo comprimento das \textit{streamlines}, a quantidade de pontos da nuvem e o ponto central. Vale ressaltar que a unidade padrão do ParaView é o $m$.

O OpenFOAM\textregistered \ possui um \textit{framework} de funções de pós-processamento, uma dessas funções é a \textit{singleGraph}. Ela grava dados de campos especificados pelo usuário na extensão de uma linha que cruza o domínio espacial, e o faz num arquivo de 
texto onde a primeira coluna diz respeito à variável espacial (tomando o comprimento da linha) e as colunas adjacentes referem-se aos valores das componentes do campo (na sequência $x$, $y$, $z$). A linha de referência é especificada a partir de dois pontos, um inicial e outro final, definidos nas linhas 12 e 13 do Cod. \ref{singleGraph}.

\begin{algorithm}[!ht]
	\caption{Código do utilitário \textit{singleGraph}}
	\label{singleGraph}
	\hrulefill
	\begin{lstlisting}
FoamFile
{
  version     2.0;
  format      ascii;
  class       dictionary;
  object      graph;
}

graph
{
  start   (0.002 0.00512 0.001);
  end     (0.002 0.00742 0.001);

  fields  ( U p );
  #includeEtc "caseDicts/postProcessing/graphs/sampleDict.cfg"
  #includeEtc "caseDicts/postProcessing/graphs/graph.cfg"
}
	\end{lstlisting}
	\vspace{-0.25cm}
	\hrulefill
\end{algorithm}

Para que os dados sejam gerados é necessário copiar o arquivo ``\textit{singleGraph}'' contido no endereço $\backslash etc \backslash caseDicts \backslash postProcessing \backslash graphs$ no diretório de instalação do OpenFOAM e adicioná-lo na pasta ``\textit{system}'' do seu caso problema. Depois, deve-se incluí-lo nas funções do arquivo ``\textit{controlDict}'' antes do processamento iniciar. A sintaxe pode ser vista no Cod. \ref{controlDict}.

\begin{algorithm}[!ht]
	\caption{Trecho do arquivo \textit{ControlDict} onde são definidos utilitários}
	\label{controlDict}
	\hrulefill
\begin{lstlisting}
functions
{	
  #include "singleGraph"
  
  residuals
  {
   fields ( p U );
   #includeEtc "caseDicts/postProcessing/numerical/residuals.cfg"
  }
}
\end{lstlisting}
	\vspace{-0.25cm}
	\hrulefill
\end{algorithm}

É neste arquivo ``\textit{controlDict}'' que estão as configurações do processamento. No Cod. \ref{controlDict} as linhas de 5 à 9 tratam  do utilitário ``\textit{residuals}'', que armazena o erro nos cálculos dos campos de pressão (p) e velocidade (U) ao longo das iterações. Todos os arquivos de saída, tanto de \textit{singleGraph} quanto de \textit{residuals} são escritos no diretório ``\textit{postProcessing}''. Os dados podem ser plotados utilizando o \textit{gnuplot}. 

