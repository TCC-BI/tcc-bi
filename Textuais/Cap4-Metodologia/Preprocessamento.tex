\section{Pré-Processamento} \label{secao:preprocessamento}

Na seção \ref{secao:equacoes} foram apresentadas as equações que governam o fenômeno ora estudado e na seção \ref{sec:discretizacao} foi mostrada
a discretização destas equações através do Método dos Volumes Finitos. A atual seção manterá o foco no processo de confecção da geometria, na
discretização do domínio (geração da malha) e nas condições de contorno usadas na simulação do fenômeno.  

\subsection{Geometria 2D} \label{subsec:geometria}

O objeto de estudo do presente trabalho é a simulação computacional do escoamento sanguíneo na bifurcação da artéria carótida, apresentada na seção \ref{secao:arteria}.
A geometria da artéria foi obtida a partir de \citeonline{bib:beleza}, uma dissertação de mestrado que simula o mesmo fenômeno, porém discretizando as equações 
governantes com o Método das Diferenças Finitas (MDF). Os resultados obtidos via MVF serão confrontados com os obtidos por \citeonline{bib:beleza} com o MDF. A geometria 
da artéria, já com a malha gerada, é mostrada na Fig. \ref{fig:geometria}.

\begin{figure}[htb!]{11cm}
	\caption{Geometria e malha bidimensionais}
	\includegraphics[scale=0.65]{geometria.png}
	\legend{Malha triangular com $3.627$ células.}
	\source{Geometria obtida a partir dos trabalhos de \citeauthoronline{bib:beleza}, \citeyear{bib:beleza}.} 
	\label{fig:geometria}
\end{figure}

Para o desenho da geometria foi utilizado o \textit{software} livre \textit{FreeCAD}, o passo a passo pode ser conferido no Apêndice \ref{freeCad}. 

\begin{citacao}
O \textit{FreeCAD} é um modelador $3D$ paramétrico \textit{free}, de código livre, feito principalmente para projetar objetos da vida real [$\cdots$]. O \textit{FreeCAD} 
é livre para fazer \textit{download}, usar, distribuir e modificar, além disso seu código fonte é aberto e público [$\cdots$]. \textit{FreeCAD} é também fundamentalmente 
um projeto social, uma vez que ele é desenvolvido e mantido por uma comunidade de desenvolvedores e usuários [$\cdots$] \footnote{Texto original: ``FreeCAD is a free, 
open-source parametric $3D$ modeling aplication. It is made primarily to model real-world objects [$\cdots$]. FreeCAD is free to download, use, distribute and modify, 
and its source code is open and published [$\cdots$]. FreeCAD is also fundamentally a social project, as it is developed and maintained by a community of developers 
and users [$\cdots$].''} \cite[tradução nossa]{bib:havre}.  
\end{citacao}

Mesmo para a simulação bidimensional, na qual os cálculos consideram o plano $xy$, o  OpenFOAM\textregistered \ exige uma geometria em três dimensões, sendo que a opção por uma simulação bidimensional ou tridimensional é feita através da adequada escolha das condições de fronteira, o que será abordado mais adiante. Tendo 
em vista isso, foi realizada uma extrusão de $0.1$ no eixo $z$ apenas para atender a exigência do \textit{software} por uma geometria $3D$. O reultado final, após a
extrusão, é a geometria extrudada na Fig. \ref{fig:freecad} do apêndice \ref{freeCad}.    


\subsubsection{Malha da geometria 2D} \label{subsec:malha}

Para a discretização do domínio, visto na Fig. \ref{fig:geometria}, optou-se pelo \textit{software} livre \textit{Gmsh}. Para tanto foi necessário exportar a
geometria do \textit{FreeCAD} em um formato que o \textit{Gmsh} fosse compatível, tal formato é o \textit{.step}. O \textit{Gmsh} é ``um gerador de malha 
tri-dimensional [$\cdots$]. Seu objetivo é prover uma ferramenta de geração de malha rápida, leve e de interface amigável com entradas paramétricas e avançadas 
capacidades de visualização'' \footnote{Texto original: ``Gmsh is a three-dimensional finite element grid generator [$\cdots$]. Its design goal is to provide a fast, 
light and user-friendly meshing tool with parametric input and advanced visualization capabilities.''} \cite[tradução nossa]{bib:geuzaine2}. O \textit{software} é ``construído em 
torno de quatro módulos: geometria, malhas, \textit{solver} e pós-processamento. As instruções, nos quatro módulos,  podem ser dadas ao\textit{ Gmsh} tanto através da interface
gráfica como em arquivos de texto'' \footnote{Texto original: ``Gmsh is built around four modules: geometry, mesh, solver and post-processing instructions are prescribed either 
interactively using the graphical user interface (GUI) or in text files.''} \cite[tradução nossa]{bib:geuzaine}. 

No módulo de geração de malhas, aqui utilizado, primeiro importou-se o arquivo \textit{.step} construído no FreeCAD. Após isso, antes da criação da malha em si,
é necessário dar nome a cada uma das faces da geometria; este passo é fundamental, pois os nomes atribuídos identificarão as fronteiras da geometria e serão utilizados 
no momento de definir as condições de contorno do problema, no OpenFoam\textregistered. Cada face, após nomeada, é reconhecida como um grupo físico pelo \textit{Gmsh}; 
estes grupos físicos são mostrados na Fig. \ref{fig:nomes}.

\begin{figure}[htb!]{14cm}
	\caption{Nomes atribuídos a cada fronteira da geometria}
	\includegraphics[scale=0.5]{gruposfisicos.png}
	\label{fig:nomes}
	%\legend{Optou-se por mostrar a figura em \textit{2D} para facilitar a visualização dos grupos físicos.}
	\source{Os autores, 2017.}
\end{figure}

O grupo físico nomeado como \textit{inlet} \footnote{Entrada, em tradução literal.} corresponde a entrada da geometria, ao início do domínio computacional; equivale a
uma seção transversal da artéria carótida comum. O grupo denominado \textit{outlet1} \footnote{Saída 1, em traducão literal.} corresponde a uma das extremidades de 
saída da geometria, equivalente a uma seção transversal da artéria carótida externa, assim como o grupo \textit{outlet2} \footnote{Saída 2, em traducão literal.} 
equivale a seção transversal da artéria carótida interna. As fronteiras superior, inferior e da bifurcação foram reunidas em um mesmo grupo físico chamado de \textit{walls} 
\footnote{Paredes, em tradução literal.}; a reunião destas fronteiras em um único grupo se justifica pelo fato delas apresentarem as mesmas características físicas em relação 
ao escoamento, como será confirmado adiante quando tratar-se das condições de contorno. As duas faces restantes, que correspondem a região anterior e posterior da artéria, 
foram nomeadas \textit{topAndBottom} \footnote{Topo e fundo, em tradução literal.}. 

Por fim, é necessário também criar um grupo físico que englobe o volume total da geometria, que foi chamado de \textit{internalMesh} \footnote{Malha interna,em tradução 
literal.}. Este grupo é necessário porque as malhas são geradas para cada grupo físico, logo se este não fosse criado o \textit{software} geraria apenas malhas nas faces
da geometria, sem nenhuma malha em seu interior. Após criados os grupos físicos, o \textit{Gmsh} exige que o arquivo seja salvo no formato \textit{.geo}, próprio dele. 
A partir deste arquivo que serão geradas as malhas. 

O processo de geração de malhas em si, pelo \textit{Gmsh}, é simples. Primeiro deve-se configurar os parâmetros desejados para a malha, o que é feito selecionando-se a opção \textit{Tools} no menu localizado no topo da interface gráfica (ver Fig. \ref{fig:gmsh}) e clicando na subopção \textit{Options}; isto abrirá uma pequena aba onde é possível configurar os parâmetros da malha.
O parâmetro mais relevante é o fator de refinamento da malha, que determinará o número de células da mesma, que na aba que se abriu corresponde ao campo \textit{Element size factor}. 
O fator de refinamento, no \textit{Gmsh}, é um número entre $0$ e $1$, em que quanto mais próximo de zero o valor, mais refinada é a malha. Para a geração da malha foi utilizado o 
fator $0,\!1$, o que levou a geração de um total de $3.627$ células (ver Fig. \ref{fig:geometria}). Por padrão, o \textit{Gmsh} gera malhas não-estruturadas e triangulares, padrão 
este que foi adotado neste trabalho. Após configurados estes parâmetros, basta ir no menu localizado à esquerda da interface gráfica, clicar na opção \textit{Mesh} e depois, 
dentre as opções que vão surgir, clicar em $3D$. Após isso é só aguardar o \textit{software} realizar a geração e salvar a malha com a extensão \textit{.msh}. A malha final 
pode ser visualizada nas Figs. \ref{fig:geometria} e \ref{fig:gmsh}.

\subsubsection{Condições de Contorno da geometria 2D} \label{condcontorno}

A última etapa relacionada ao pré-processamento é realizada já no simulador, no OpenFOAM, e consiste em definir as condições de contorno do problema. Os arquivos 
com as condições de contorno estão localizados dentro da pasta ``$0$'' do caso problema. Dentro da pasta há um arquivo chamado ``\textit{p}'', que armazena as condições de contorno 
relativas à pressão e outro chamado ``\textit{U}'', que armazena as condições relacionadas com a velocidade. Nos anexos \ref{anexA} e \ref{anexB} encontram-se a íntegra destes dois 
arquivos, respectivamente. 

As condições de contorno definidas para cada fronteira, utilizadas na solução dos campos de pressão e velocidade, são sintetizadas na Tab. \ref{tab:contorno}.

\setlength{\tabcolsep}{15pt}
\renewcommand{\arraystretch}{1.8}
\begin{table}[!ht]{14cm}
	\caption{Condições de contorno do problema 2D}
	\label{tab:contorno}
	\hfill \begin{tabular}{ccc}
		\Xhline{2\arrayrulewidth}
		\multirow{2}{*}{\textbf{Fronteira}} & \multicolumn{2}{c}{\textbf{Condições de Contorno}}                                                                                      \\ \cline{2-3} 
		& \textbf{Pressão}                  & \textbf{Velocidade}                                       \\ \Xhline{2\arrayrulewidth}
		inlet                               &  \parbox[t]{10em}{\centering $\nabla p =0$}                    &  \parbox[t]{10em}{\centering $f(y)$}                        \\
		outlet1                             & $p = 0$                                                        & $\nabla \vec{U} = 0$                                                            \\
		outlet2                             &  $p = 0$                                                       & $\nabla \vec{U} = 0$                                                           \\
		walls                               & $\nabla p = 0$                                               & $\vec{U} = 0$                        \\
		topAndBottom                        & empty                                                          & empty                                                                   \\ 
		\Xhline{2\arrayrulewidth}
	\end{tabular} \hfill
	\source{Os autores, 2017.}
\end{table}

Nas fronteiras \textit{inlet} e \textit{walls} foi aplicada a condição $\nabla p = 0$ à pressão, que simplesmente calcula o valor da grandeza na fronteira extrapolando os valores do campo interno. Nas extremidades da \textit{acc} e \textit{aci} fixou-se a pressão em $0$. Quanto a fronteira \textit{topAndBottom}, como trata-se de uma simulação bidimensional, é necessário informar ao OpenFOAM\textregistered \, para desconsiderá-la na simulação, isto é feito através da condição \textit{empty}, própria do OpenFOAM\textregistered.

Quanto a velocidade, na fronteira \textit{inlet} utilizou-se uma biblioteca denominada \textit{groovyBC}, que permite definir uma condição de contorno não-uniforme $f(y)$, adotando-se no presente caso um perfil de velocidade parabólico, caracterizando o escoamento desde seu início como completamente desenvolvido; na fronteira \textit{walls} foi fixada velocidade nula nas paredes. Nas saídas, \textit{outlet1} e \textit{outlet2}, foi utilizada a condição $\nabla p = 0$, extrapolando-se a velocidade nessas regiões. E, pelas mesmas razões supracitadas quando falou-se da pressão, a fronteira \textit{topAndBottom} recebeu a condição \textit{empty}.

\subsubsection{Geometria e Malha $3D$}

Procurou-se simular um modelo real tridimensional de uma Artéria carótida humana, conseguida a partir de uma técnica de digitalização chamada em inglês de ``\textit{scan of a lluminal casting}'', que pode ser encontrada em  \url{https://grabcad.com/library/carotid-bifurcation-2}.

Para a geração da malha da geometria em $3D$ utilizou-se um utilitário fornecido juntamente com o próprio OpenFOAM\textregistered, o \textit{snappyHexMesh}. O processo detalhado de como este utilitário realiza a geração de malhas pode ser encontrado na seção $5.4$ de \citeonline{bib:greenshields}, mas em suma se dá da seguinte maneira:

\begin{enumerate}
	\item Devem ser fornecidos ao \textit{snappyHexMesh}, como \textit{input}, uma superfície triangulada (ou \textit{triSurface}), que pode ser em formato \textit{.stl} ou \textit{.obj}; também deve ser fornecido uma ``malha de fundo'' \footnote{\textit{background mesh} no original, em inglês.}. 
	
	\item Também é necessário um arquivo, denominado \textit{snappyHexMeshDict}, localizado na pasta \textit{system} do caso problema, onde constarão todas as especificações para a geração da malha. O arquivo \textit{snappyHexMeshDict} do problema em estudo pode ser visto na íntegra no Anexo \ref{anexD}. A Fig. \ref{fig:snappyFigure} mostra os \textit{inputs} supracitados no item 1.
	
	\begin{figure}[!H]{13cm}
		\caption{Superfície STL e malha de fundo}
		\includegraphics[scale=0.7]{snappyFigure.png}
		\label{fig:snappyFigure}
		\legend{A região mais clara é uma superfície STL que representa a Artéria Carótida, já a mais escura, de formato retangular, corresponde a malha de fundo citada anteriormente.}
		\source{Os autores, 2018.}
	\end{figure}
	
	\item O passo seguinte no processo de geração da malha consiste em iniciar o refinamento da malha de fundo aos níveis determinados no arquivo \textit{snappyHexMeshDict}, sendo que neste estágio o utilitário manterá apenas as células da malha de fundo que estejam intersectadas com a superfície STL, eliminando as demais. Chegando-se então ao resultado mostrado na Fig. \ref{fig:framesvert2D}.
	
\end{enumerate}

Para a obtenção da superfície STL e da malha de fundo foi usado o \textit{software} livre SALOME, uma plataforma que possui ferramentas de pré e pós-processamento. 
Mais informações sobre o SALOME podem ser encontradas em \url{http://www.salome-platform.org/}. 

No caso em tela, tanto a superfície STL quanto a malha de fundo foram geradas com o SALOME. No sítio supracitado obteve-se a geometria tridimensional 
no formato \textit{.step}, que foi importada para o SALOME, onde foram definidas as fronteiras da geometria conforme disposto na Fig. \ref{fig:gruposfisicos3d} e, 
conseguintemente, exportou-se a geometria com a extensão \textit{.stl}, devendo esta ser salva na pasta \textit{triSurface}, localizada dentro da pasta \textit{constant} no diretório do caso problema.
\begin{figure}[!H]{14cm}
	\caption{Geometria e malha tridimensionais} \label{fig:framesvert2D}
	\subfloat[][Vista tridimensional]{\label{carotida3d}
		\includegraphics[scale=0.56]{Carotida3D}}\\
	%\subfloat[][Vista frontal]{\label{carotidafrontal}
	%	\includegraphics[scale=0.2]{Carotidafrontal3D}}\\
	\subfloat[][Vista lateral]{\label{carotidalateral}
		\includegraphics[scale=0.50]{Carotidalateral3D}}
	%\subfloat[][Carotidasuperior3D]{\label{carotidasuperior}
	%	\includegraphics[scale=0.5]{Carotidasuperior3D}}  
	\legend{Malha mista com $16.661$ células criada com o utilitário \textit{snappyHexMesh} do OpenFOAM\textregistered.}  
	\source{Os autores, 2018.}
\end{figure}
Quanto a malha de fundo, por se tratar de uma geometria simples, esta foi gerada diretamente no SALOME, sendo exportada em seguida com a extensão \textit{.unv}. 
Após isso, no terminal do OpenFOAM\textregistered, deve ser dado o comando \textit{ideasUnvToFoam} que converterá a malha de fundo com extensão \textit{.unv} para o
formato nativo do OpenFOAM\textregistered. Depois desta etapa, basta executar o comando \textit{snappyHexMesh} no terminal para que a malha seja gerada.

\begin{figure}[!H]{10cm}
	\caption{Nomes atribuídos a cada fronteira da geometria}
	\includegraphics[scale=0.5]{gruposfisicos3D.png}
	\label{fig:gruposfisicos3d}
	\legend{Grupos físicos, eles correspondem as fronteiras que receberão condições de contorno no código.}
	\source{Os autores, 2018.}
\end{figure}
%
\subsubsection{Condições de contorno da geomtria $3D$} \label{subsubsec: condicoes3D}

As condições de contorno, aplicadas sobre cada fronteira durante o processamento, são sintetizadas na Tab. \ref{tab:contorno3d}. Pode-se fazer uma analogia às condições de contorno do caso 2D, mas diferentemente do que se observa na Tab. \ref{tab:contorno}, não há na Tab. \ref{tab:contorno3d} condição de contorno do tipo \textit{empty}, pois agora o processamento é tridimensional. 

Além disto, o formato da artéria está definido em um único grupo físico, denominado \textit{pipewall}. Esta fronteira recebe a condição de contorno \textit{noSlip}, também conhecida como condição de não deslizamento, que é aplicada em sólidos (ou paredes).

\begin{table}[!ht]{14cm}
	\caption{Condições de contorno do problema $3D$}
	\label{tab:contorno3d}
	\hfill \begin{tabular}{ccc}
		\Xhline{2\arrayrulewidth}
		\multirow{2}{*}{\textbf{Fronteira}} & \multicolumn{2}{c}{\textbf{Condições de Contorno}}                                                                                      \\ \cline{2-3} 
		& \textbf{Pressão}                                               & \textbf{Velocidade}                                                    \\ \Xhline{2\arrayrulewidth}
		inlet                               &\parbox[t]{10em}{\centering $\nabla p =0$}                                                    & \parbox[t]{10em}{\centering $f(y)$}                        \\
		outlet1                             & $p = 0$            & $\nabla \vec{U} = 0$                                                            \\
		outlet2                             & $p = 0$            & $\nabla \vec{U} = 0$                                                            \\
		pipewall                               & $\nabla p = 0$                                                   & \parbox[t]{10em}{\centering não deslizamento}                        \\
		 
		\Xhline{2\arrayrulewidth}
	\end{tabular} \hfill
	\source{Os autores, 2018.}
\end{table}

\subsection{Propriedades de transporte}

No presente trabalho, o sangue foi simulado como um fluido newtoniano, pois os requisitos que tornam tal simplificação possível (descritos na seção
\ref{secao:principios}) estão presentes na região que será simulada, isto é, os diâmetros da \textit{acc}, \textit{aci} e \textit{ace} possuem 
ordem de grandeza elevada em relação as células sanguíneas que escoam nas mesmas, o que torna os efeitos inerciais bem maiores que os efeitos
viscosos. Utilizou-se uma viscosidade cinemática de $3,7 \times 10^{-6} \, m^{2}/s$, o que leva a $Re=250$, ou seja, um escoamento laminar; 
este é o mesmo número de Reynolds utilizado por \citeonline{bib:beleza} em sua simulação. No Código \ref{proptransp} são exibidas as propriedades de transporte utilizadas na simulação.

\begin{algorithm}[!ht]
	\caption{Propriedades de transporte}
	\label{proptransp}
	\hrulefill
	\begin{lstlisting}
	FoamFile
	{
	version     2.0;
	format      ascii;
	class       dictionary;
	location    "constant";
	object      transportProperties;
	}
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
	
	transportModel  Newtonian;
	
	rho 		[1 -3 0 0 0 0 0] 1060;
	
	nu              [0 2 -1 0 0 0 0] 3.7e-06;
	\end{lstlisting}
	\vspace{-0.25cm}
	\hrulefill
\end{algorithm}

As simulações foram realizadas considerando-se o escoamento sanguíneo em regime permanente com $\rho = 1060 \, kg/m^{3}$ (incompressível). O fluxo sanguíneo através do corpo humano têm um comportamento intrinsecamente transiente, devido sua natureza pulsátil, entretanto, tendo em vista a carga de complexidade envolvida na modelagem do problema transiente optou-se aqui por simular o fluxo considerando-o em regime permanente. Os demais parâmetros de simulação podem ser encontrados no Anexo \ref{anexC}.
  
