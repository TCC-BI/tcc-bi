\section{Processamento} \label{secao:processamento}

A aplicação do Método dos Volumes Finitos foi concretizada com o \textit{software} livre OpenFOAM\textregistered. Segundo \citeonline{bib:greenshields}, o OpenFOAM\textregistered \, é uma biblioteca de C++, usada primariamente para criar executáveis, que se dividem em duas categorias: \textit{solvers}, desenvolvidos para resolver um problema específico da mecânica do contínuo; e utilitários, que objetivam realizar tarefas que envolvem manipulação de dados. No próprio software já há diversos \textit{solvers} e utilitários, além de vários \textit{case files} prontos para teste, os quais o usuário, com um editor adequado, pode modificar. Segundo \citeonline{bib:jatoba}, o OpenFOAM (Field Operation and Manipulation \footnote{Algo como Operação e Manipulação de Campos, em Português}) se destaca entre os pacotes de DFC pela maturidade do código livre e pela liberdade de implementação disponível em sua estrutura. A Fig. \ref{fig:openfoam} nos permite ter uma visão geral da estrutura do OpenFOAM\textregistered. 

\begin{figure}[htb!]{15cm}
	\caption{Visão geral da estrutura do OpenFOAM\textregistered}
	\includegraphics[scale=0.6]{openfoam.png}
	\label{fig:openfoam}
	\source{Adaptada de \citeauthoronline{bib:greenshields}, \citeyear{bib:greenshields}.}
\end{figure}

Como mostra a Fig. \ref{fig:openfoam}, o OpenFOAM\textregistered \, possui ferramentas que compreendem as três etapas do processo de simulação. No que se refere ao \textit{pré-processamento} ele traz ferramentas próprias de geração e manipulação de malhas, tais como o \textit{BlockMesh} e o \textit{snappyHexMesh}, todavia, neste trabalho optou-se, como já dito, pela geração da malha no \textit{Gmsh}, tornando-se necessário converter a malha gerada externamente para o formato requisitado pelo OpenFOAM\textregistered. Para esta tarefa de conversão existem diversos utilitários implementados no simulador, dentre eles o \textit{gmshToFoam}, desenvolvido especificamente para converter malhas oriundas do \textit{Gmsh}. 

Para realizar a conversão, basta copiar o arquivo \textit{.msh} gerado pelo \textit{Gmsh} para o diretório com os arquivos do caso a ser simulado. Após isso, deve-se digitar na janela do OpenFOAM\textregistered \, o seguinte comando

\begin{lstlisting}[numbers=none]
$ gmshToFoam nome.msh
\end{lstlisting}

\noindent em que \textit{nome} se refere ao nome com o qual o arquivo \textit{.msh} foi salvo. Após a conversão da malha, foi utilizado outro utilitário, o \textit{checkMesh}, destinado a verificar a validade e integridade da malha antes que se inicie a simulação; para acionar o utilitário basta utilizar o comando

\begin{lstlisting}[numbers=none]
$ checkMesh
\end{lstlisting}

\noindent como este utilitário não retornou nenhum erro relacionado a malha, continuou-se com o procedimento de simulação. A próxima etapa é a simulação propriamente dita, o processamento. 

Para esta etapa, onde ocorre a solução das EDP's governantes do problema, foi utilizado o \textit{solver} simpleFoam, descrito oficialmente como um ``\textit{solver} de regime permanente para escoamentos incompressíveis, turbulentos'' \footnote{Texto original: ``Steady-state solver for incompressible, turbulent flow''} \cite[tradução nossa]{bib:greenshields}. Este \textit{solver} permite resolver escoamentos permanentes, incompressíveis, isotérmicos, tanto laminares como turbulentos utilizando o Método dos Volumes Finitos \cite{bib:moreira}.

O simpleFoam resolve as equações governantes através do algoritmo SIMPLE (\textit{Semi-Implicit Method for Pressure-Linked Equations} \footnote{Algo como Método Semi-Implícito para Equações Acopladas à Pressão, em Português}), que foi implementado inicialmente por Patankar e Spalding, em 1972 \cite{bib:moukalled}. Como o nome sugere, este \textit{software} faz o acoplamento da velocidade com a pressão, resolvendo as equações com o termo da densidade e da viscosidade constantes. 

No OpenFOAM\textregistered, as equações governantes podem ser discretizadas implícita ou explicitamente, a depender do \textit{solver}. De acordo com \citeonline{bib:jatoba}, na discretização implícita os operadores diferenciais, como o divergente e o laplaciano, formam um sistema de equações lineares que representam a discretização da EDP na malha, em que o sistema de equações formado precisa ser resolvido para determinar a solução da variável no campo no instante de tempo seguinte. Ainda segundo esta autora, na discretização explícita o operador vetorial é calculado considerando o resultado da operação usando o valor presente da variável na malha.

O simpleFoam adota um esquema semi-implícito de discretização. Os passos de solução do algoritmo estão sintetizados a seguir:

\begin{enumerate}
	\item Uma aproximação do campo de velocidade é obtida resolvendo-se a Equação do \textit{momentum}. O termo do gradiente de pressão é calculado usando a distribuição de pressão da iteração anterior ou um chute inicial;
	\item A equação da pressão é formulada e solucionada para se obter a nova distribuição de pressão;
	\item Velocidades são corrigidas e uma nova série de fluxos conservativos é calculada.
\end{enumerate}
 
A equação do \textit{momentum} discretizada e a equação de correção da pressão são resolvidas implicitamente, e a equação da velocidade é resolvida explicitamente. Esta é a razão do algoritmo ser chamado de semi-implícito. 

\begin{algorithm}[!ht]
	\caption{Trecho do \textsc{simpleFoam} que resolve a Equação da Conservação Momento (Eq. \eqref{eqconservacaomomentovetorial})}
	\label{momentumpredictor}
	\hrulefill
	\begin{lstlisting}
	tmp<fvVectorMatrix> UEqn
	(
	fvm::div(phi, U)
	+ MRF.DDt(U)
	+ turbulence->divDevReff(U)
	==
	fvOptions(U)
	);
	solve(UEqn() == -fvc::grad(p));
	\end{lstlisting}
	\vspace{-0.25cm}
	\hrulefill
\end{algorithm}
