\chapter{Resultados} \label{chapter:resultados}

A bifurcação da carótida possui características geométricas tidas como centrais na formação do fluxo sanguíneo na região, tais como: a presença de efeitos de curvatura nas junções artéria 
carótida comum–artéria carótida interna e artéria carótida comum–artéria carótida externa; exis\-tência de um alargamento do diâmetro na entrada da artéria carótida interna, o \textit{sinus}, que abruptamente se estreita na saída; e assimetria quanto as ramificações das artérias interna e externa (a carótida interna tem um diâmetro maior). O \textit{sinus}, particularmente, é uma região crítica para o fluxo sanguíneo local devido ao seu alargamento e posterior afunilamento, exibindo propriedades de um duto curvo na transição da artéria carótida comum para a artéria carótida interna. 

Após o início da simulação a convergência foi alcançada depois de $78$ iterações. Com a simulação tendo alcançado a convergência foi utilizado o ParaView, conforme mencionado, para visualizar
as linhas de corrente, o campo de velocidades, campo de pressão e gráfico de resíduos, que serão mostrados mais adiante. A Fig. \ref{fig:linhacorrente} mostra as linhas de corrente do escoamento 
obtidas em \citeonline{bib:beleza} e as linhas de corrente obtidas neste trabalho. 

\begin{figure}[htb!]{15cm}
	\caption{Linhas de corrente do escoamento para Re = 250} \label{fig:linhacorrente}
	\subfloat[][]{\label{fig:linhacorrentebeleza} \fbox{\includegraphics[scale=0.5]{linhacorrentebeleza.png}}}
	\subfloat[][]{\label{fig:linhacorrenteobtida} \fbox{\includegraphics[scale=0.36]{linhacorrente}}}\\
	\legend{\subref{fig:linhacorrentebeleza} Linhas de escoamento obtidas por \citeonline{bib:beleza} através do Método das Diferenças Finitas.
		    \subref{fig:linhacorrenteobtida} Linhas de corrente obtidas pelos autores através do Método dos Volumes Finitos.}
	\source{\subref{fig:linhacorrentebeleza} \citeauthoronline{bib:beleza}, \citeyear{bib:beleza}.
	        \subref{fig:linhacorrenteobtida} Os autores, 2017.}
\end{figure}

Observa-se uma compatibilidade entre as Figs. \ref{fig:linhacorrentebeleza} e \ref{fig:linhacorrenteobtida}, valendo observar que a Fig. \ref{fig:linhacorrenteobtida}, resultado das
simulações realizadas neste trabalho, apresenta um número bem maior de linhas de corrente, o que foi possível graças as poderosas ferramentas de visualização do ParaView. A presença 
dos mesmos padrões de escoamento em ambas as imagens aponta para a compatibilidade, neste aspecto, entre os resultados obtidos aqui via MVF com os resultados obtidos por \citeonline{bib:beleza} via MDF. 

As linhas de corrente comprovam que nas zonas de geometria complexa, supracitadas, o escoamento apresenta comportamento crítico. Quanto a assimetria das ramificações, na bifurcação 
da artéria o fluxo se divide numa proporção de 40\% para a artéria carótida externa e 60\% para a artéria carótida interna (Beleza, 2003). Através das linhas de corrente observa-se 
que nas ramificações o escoamento se torna mais complexo: tanto na artéria carótida interna (ramo inferior na Fig. \ref{fig:linhacorrente}) como na externa (ramo superior na Fig.~\ref{fig:linhacorrente}) 
há a presença de fluxo reverso, as chamadas recirculações, que se apresentam como redemoinhos em meio ao escoamento.

\begin{figure}[H]{16cm}
	\caption{Campo de velocidades obtido neste trabalho, para Re=250}
	\includegraphics[scale=0.63]{campovelocidade.png}
	\source{Os autores, 2017.} 
	\label{fig:campovelocidade}
\end{figure}

 Essas recirculações se formam nas paredes não divisoras (aquelas mais externas) das junções carótida comum–carótida interna e carótida comum–carótida externa, onde a 
 geometria apresenta propriedades de um duto curvo. As implicações dessas recirculações serão discutidas adiantes quando forem mostrados os campos de velocidade e pressão.
 A Fig. \ref{fig:campovelocidade} apresenta o campo de velocidade obtido no projeto.
 

No campo de velocidade obtido por \citeonline{bib:beleza}, o perfil de velocidade no decorrer da artéria carótida comum apresenta-se totalmente desenvolvido e simétrico, não havendo 
influência da bifurcação no comportamento da velocidade; tal situação se reflete no gráfico do perfil de velocidade perfeitamente parabólico obtido pelo referido autor 
na \textit{acc}. Neste trabalho, contudo, conforme mostra a Fig. \ref{fig:campovelocidade}, o perfil de velocidade obtido na \textit{acc} não foi uma parábola perfeita, revelando um comportamento assimétrico da velocidade em relação ao eixo central da artéria. 

Próximo as paredes da \textit{acc} observa-se uma esperada redução da velocidade, perceptível através da escala de cores. De fato, observa-se um gradiente de velocidade
próximo as paredes da artéria comum que, conforme se verá a seguir, também existe nas paredes das ramificações de forma mais acentuada. Neste aspecto, há concordância com os resultados de \citeonline{bib:beleza}.

Na região da bifurcação em si, as velocidades diminuem, devido à influência do alargamento da artéria carótida comum (Beleza, 2003). Observa-se que a velocidade mantém-se 
menor, dentro de cada ramificação, do que aquelas presentes na \textit{acc}.

É interessante notar que as recirculações mostradas na Fig. \ref{fig:linhacorrente} correspondem as regiões com as velocidades mais baixas na Fig. \ref{fig:campovelocidade} 
(região azul escura). De fato, o escoamento na entrada da artéria carótida interna apresenta comportamento complexo observando-se alguns fenômenos como, por exemplo, a redução
de velocidade na região do \textit{sinus} e a formação de recirculação com baixas velocidades na parede não divisora. Estes efeitos que ocorrem no \textit{sinus} se devem a sua
geometria altamente irregular, com a presença de alargamento e afunilamento abruptos; segundo \citeonline{bib:beleza}, o fato de o \textit{sinus} possuir um diâmetro maior que a \textit{acc} e, 
ao mesmo tempo, ter uma menor vazão para a \textit{aci} favorece o surgimento das zonas de recirculação.

Ainda quanto à \textit{aci}, observa-se também na Fig. \ref{fig:campovelocidade} o gráfico do perfil de velocidade no início do \textit{sinus}. Tal gráfico apresenta o mesmo 
comportamento daquele obtido por \citeonline{bib:beleza}, sendo coerente com o que se observa através da escala de cores: na parte do \textit{sinus} próxima a parede não-divisora, onde se 
formam as recirculações, a velocidade chega a ser negativa, o que condiz com a condição de fluxo reverso; após isso a velocidade aumenta gradativamente ao se afastar da parede 
não-divisora e passando a diminuir em seguida, aproximando-se da parede divisora. Também há a presenças de gradientes de velocidade em ambas as paredes desta ramificação, que 
são maiores que aqueles presentes na carótida comum, outra consequência da geometria irregular.

Quanto à artéria carótida externa, o perfil de velocidade apresenta basicamente as mesmas características da carótida interna, mais de forma suavizada, reduzida. Verifica-se a 
presença de gradientes de velocidade em ambas as paredes, havendo a presença de uma zona de recirculação proporcionalmente menor em relação àquela presente no sinus. O perfil 
de velocidade da \textit{ace} apresentado na Fig. \ref{fig:campovelocidade} também apresenta concordância com o obtido por \citeonline{bib:beleza} para esta ramificação, verificando-se 
a presença de valores negativos da velocidade na região de fluxo reverso. 

Na sequência da análise dos resultados a Fig. \ref{fig:campopressao} apresenta o campo de pressão obtido  para o escoamento. Tal campo não foi extraído nem avaliado por \citeonline{bib:beleza}.
 
\begin{figure}[htb!]{13cm}
	\caption{Campo de pressao obtido neste trabalho, para Re=250}
	\includegraphics[scale=0.65]{campopressao.png}
	\source{Os autores, 2017.} 
	\label{fig:campopressao}
\end{figure}

A Fig. \ref{fig:campopressao} mostra o campo de pressão do escoamento. Importante salientar que trata-se aqui da pressão dinâmica, definida como a pressão estática dividida 
pela massa específica do fluido. Percebe-se que na entrada da carótida comum a pressão é maior e vai diminuindo ao longo da artéria até chegar na bifurcação, onde, devido ao
alargamento abrupto da artéria há uma queda ainda maior da pressão. Contudo, nos arredores do vértice da bifurcação há um aumento da pressão, sendo que a pressão máxima do 
escoamento é exatamente no vértice, como mostra a cor vermelha, o que era esperado tendo em vista que neste ponto há a divisão do fluxo.

Nota-se ainda que nas duas zonas de recirculação a pressão é menor em relação a maior parte do escoamento; este decréscimo de pressão juntamente com as baixas velocidades nas recirculações levam a diminuição da tensão de cisalhamento da parede, o que propicia o acúmulo de placas e gorduras no local que futuramente acarretarão a aterosclerose, conforme 
afirmado por \citeonline{bib:beleza}.

Para corroborar os resultados alcançados pela simulação, será mostrado na Fig. \ref{fig:residuos} o gráfico de resíduos da simulação. O resíduo nada mais é do que a diferença entre 
o erro inicial (da iteração anterior) e o final (da iteração atual), então o cálculo vai sendo realizado iterativamente até que esta diferença seja menor que um determinado critério de convergência. 

\begin{figure}[htb!]{10cm}
	\caption{Gráfico de resíduos do campo de velocidade e pressão}
	\includegraphics[scale=0.6]{residuos.png}
	\source{Os autores, 2017.} 
	\label{fig:residuos}
\end{figure}

O gráfico da Fig. \ref{fig:residuos} mostra os resíduos ao decorrer das iterações para o campo de velocidade (azul) e para o campo de pressão (roxo). O critério de convergência 
utilizado foi $10^{-6}$ e, como supracitado, foram $78$ iterações até a convergência. O gráfico demonstra que houve uma queda logarítmica dos resíduos, havendo uma diminuição 
crescente dos resíduos até a convergência. O gráfico de resíduos aponta para a precisão dos resultados obtidos pela simulação, pois não houve grandes oscilações em nenhum dos 
campos durante a simulação.

\begin{figure}[htb!]{14cm}
	\caption{Linhas de corrente, para Re=250}
	\includegraphics[scale=0.6]{Linhasdecorrente3Dfinal.png}
	\source{Os autores, 2018.}
	\label{fig:linhasdecorrente3D}
\end{figure}

Quanto a geometria tridimensional (Fig. \ref{fig:framesvert2D}), utilizou-se o mesmo \textit{solver} do caso $2D$ com as condições de contorno apresentadas na 
subsubseção \ref{subsubsec: condicoes3D}, obtendo-se as linhas de corrente da Fig. \ref{fig:linhasdecorrente3D}. 

As \textit{StreamTracers} foram criadas a partir de uma nuvem com 500 pontos. Observa-se que na região do \textit{sinus}, assim como no caso $2D$ analisado ao norte, 
há a presença das zonas de recirculação, contudo o mesmo fenômeno não é observado na \textit{ace}, aventando-se a hipótese de que a não ocorrência de recirculações 
neste trecho se deva ao fato de a junção \textit{acc}-\textit{ace} na artéria da Fig. \ref{fig:framesvert2D} ser mais suave que àquela da Fig. \ref{fig:geometria}. Ressalte-se 
que as duas geometrias são diferentes, obtidas de fontes diferentes.
%\pagebreak

O campo de velocidades obtido, após a convergência da simulação, do caso tridimensional é mostrado nas Figs. \ref{fig:slice1} e \ref{fig:slice2}.
%
\begin{figure}[htb!]{11cm}
	\caption{Campo de velocidade em seção transversal da acc}
	\includegraphics[scale=0.55]{slice1.png}
	\source{Os autores, 2018.} 
	\label{fig:slice1}
\end{figure}

O campo de velocidades na \textit{acc}, ilustrado na Fig. \ref{fig:slice1}, apresenta comportamento de acordo com o esperado, isto considerando que a geometria da carótida pode ser 
classificada como um duto e também que a condição de contorno empregada na fronteira \textit{inlet} foi de um perfil parabólico (escoamento completamente desenvolvido).

\begin{figure}[htb!]{12cm}
	\caption{Campo de velocidade na seção transversal da aci}
	\includegraphics[scale=0.5]{slice2.png}\vspace{0.5cm}
	\source{Os autores, 2018.} 
	\label{fig:slice2}
\end{figure}

\pagebreak

Já a Fig. \ref{fig:slice2} ilustra o resultado na região da \textit{aci}, onde se observou mais nitidamente o fenômeno da recirculação, apresentando concordância com os 
resultados da geometria simulada em $2D$, apesar de serem artérias com formas diferentes.

\begin{figure}[H]{16cm}
	\caption{\textit{Frames} do campo de velocidades da simulação $3D$} \label{fig:framesvel3D}
	\subfloat[][$t = 1$]{\label{fig:1-U}
		\includegraphics[scale=0.4]{1-U.png}}
	\subfloat[][$t = 5$]{\label{fig:5-U}
		\includegraphics[scale=0.4]{5-U.png}}\\
	\subfloat[][$t = 8$]{\label{fig:8-U}
		\includegraphics[scale=0.4]{8-U.png}}
	\subfloat[][$t = 21$]{\label{fig:21-U}
		\includegraphics[scale=0.4]{21-U.png}}
%	\legend{As imagens ilustram o escoamento interno.}
	\source{Os autores, 2018.}
\end{figure}

A Fig. \ref{fig:framesvel3D} mostra a evolução do campo de velocidades durante a simulação. Os \textit{frames} correspondem a uma visualização do escoamento interno da carótida, 
obtida com o uso da ferramenta \textit{Clip} do \textit{ParaView}. 


Apesar da simulação ter convergido com $42$ iterações, a partir do instante de tempo $t = 21$ não houve alterações notáveis 
no comportamento do campo de velocidades. 

Em linhas gerais, pode-se afirmar que as magnitudes das velocidades axiais são altas através da região central da \textit{acc}, diminuindo conforme aproxima-se das paredes da artéria, 
e que os padrões de fluxo mudam severamente ao final da \textit{acc} devido aos efeitos de ramificação e curvatura próximos à bifurcação. 

Comparando as Figs. \ref{fig:21-U} e \ref{fig:campovelocidade} pode-se notar que os padrões de fluxo são semelhantes,  o fluido da artéria carótida comum chega com inércia na região da 
bifurcação e gera um gradiente de velocidades acentuado na direção radial. De forma que, os picos de velocidade são direcionados para a carótida interna, devido ao seu diâmetro maior. 
Assim, na \textit{aci} constata-se em ambos casos (2D e 3D) o fenômeno de recirculação. No que tange o fluxo na artéria carótida externa do caso $3D$ vê-se que este fenômeno não ocorreu, 
diferentemente do caso $2D$. E por isso, é interessante considerar que a curvatura na transição \textit{acc}-\textit{aci} é deveras suave no caso 3D, e, os diâmetros da \textit{aci} e \textit{ace} são mais distoantes neste último caso.


Na Fig. \ref{fig:framesp3D} encontram-se \textit{frames} referentes a evolução do campo de pressão (cinemática) durante a simulação.

\begin{figure}[htb!]{16cm}
	\caption{Frames do campo de pressão da simulação $3D$} \label{fig:framesp3D}
	\subfloat[][$t = 1$]{\label{fig:1-p}
		\includegraphics[scale=0.4]{1-p.png}}
	\subfloat[][$t = 3$]{\label{fig:3-p}
		\includegraphics[scale=0.4]{3-p.png}}\\
	\subfloat[][$t = 16$]{\label{fig:8-p}
		\includegraphics[scale=0.4]{16-p.png}}
	
	\source{Os autores, 2018.}
\end{figure} 

De forma semelhante ao que aconteceu para o campo de velocidades, a partir de determinado instante de tempo, no caso $t = 16$, não observou-se mais alterações notáveis no campo de 
pressão. É importante destacar na Fig. \ref{fig:8-p} que o comportamento da pressão observado é similar àquele da geometria bidimensional, mais especificamente a pressão acentuada 
na região da bifurcação (junção \textit{ace} e \textit{aci}).

Pela região de baixa pressão cinemática nas proximidades de \textit{outlet1} e \textit{outlet2} é perceptível a influência das condições de contorno da pressão cinemática aplicadas nestas fronteiras.

\begin{figure}[htb!]{10cm}
	\caption{Gráfico de resíduos do campo de velocidade e pressão}
	\includegraphics[scale=0.55]{residuos3d.png}
	\source{Os autores, 2018.} 
	\label{fig:residuos3D}
\end{figure} 

O gráfico da Fig. \ref{fig:residuos3D} mostra os resíduos ao decorrer das iterações para o campo de velocidade e para o campo de pressão, de forma análoga ao caso $2D$, entretanto, 
agora os resíduos estão especificados para cada componente da velocidade.

O critério de convergência utilizado também foi $10^{-6}$ e o cálculo convergiu em $42$ iterações. O gráfico demonstra que houve queda logarítmica dos resíduos, assim como no caso $2D$.